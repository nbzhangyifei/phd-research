import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import glob
#raw data from the paper 'Yifei Zhang, Advanced Electronic Materials, 2021'
#raw data is csv file without header.
#col0, temperature (°C)
#col1, temperature (K)
#col2, resistivity (Ohm*m)
#col3, Seebeck (V/K)
#col4, power factor (W/mK^2)
#col5, Lorenz number (1e-8*WOhm/K^2)
#col6, ke (W/mK)

##figure setting
#define font
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] 
rcParams['font.size']=12
my_color={'x = 0.00':'#e0f3db',
'x = 0.20':'#ccebc5',
'x = 0.23':'#a8ddb5',
'x = 0.25':'#7bccc4',
'x = 0.33':'#4eb3d3',
'x = 0.50':'#2b8cbe',
'x = 1.00':'#08589e'}


#subplot ax1 and ax2, they are within 1 row and 2 columns
fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharex=True, figsize=(6.4, 4), gridspec_kw={'wspace': 0.4})
#set x, y labels
ax1.set_xlabel('Temperature (°C)')
ax1.set_ylabel('Resistivity (m$\Omega$cm)')
ax2.set_xlabel('Temperature (°C)')
ax2.set_ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)')
ax2.set_ylim([-250, 10])
ax1.text(0.02,0.98,'(a)', transform=ax1.transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')
ax2.text(0.02,0.98,'(b)', transform=ax2.transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')



##read raw data
path = './data' #give the directory path for the raw data
os.chdir(path) #change the current working directory to where the raw data is located
files = glob.glob('*.csv') #get all the csv file from the path
files.sort() #sort the file according to the name
# files.sort(key=os.path.getmtime) #one can also sort file according to modified time

#plot method 2
all_data=pd.DataFrame()
#merge all data into one dataframe, add column 'sample' referring to the sample name
for i in range(0,len(files)):
        data = pd.read_csv(files[i],sep=',',header=None) #read each csv file and copy to dataframe
        data.columns=['temp_deg', 'temp_kel', 'resis', 'seeb', 'pf', 'lor_num', 'kappa_e'] #give columns names
        data['sample']=str(files[i].replace('.csv', ''))  
        all_data=pd.concat([all_data,data])
        
grouped=all_data.groupby('sample')
for key, grp in all_data.groupby(['sample']):
    ax1.scatter(grp['temp_deg'], grp['resis']*1e5, label=key, marker='o', color=my_color[key])
    ax2.scatter(grp['temp_deg'], grp['seeb']*1e6, label=key, marker='o', color=my_color[key])
    
ax2.legend(loc='lower center', bbox_to_anchor=(-0.25, 1),ncol=4, frameon=False)

plt.savefig('./subplot.pdf', bbox_inches='tight', dpi=300)
# plt.show()

