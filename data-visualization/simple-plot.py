import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import glob
#raw data from the paper 'Yifei Zhang, Advanced Electronic Materials, 2021'
#raw data is csv file without header.
#col0, temperature (°C)
#col1, temperature (K)
#col2, resistivity (Ohm*m)
#col3, Seebeck (V/K)
#col4, power factor (W/mK^2)
#col5, Lorenz number (1e-8*WOhm/K^2)
#col6, ke (W/mK)

##figure setting
plt.xlabel('Temperature (°C)')
plt.ylabel('Resistivity ($\Omega$m)')

##read raw data
path = './data' #give the directory path for the raw data
os.chdir(path) #change the current working directory to where the raw data is located
files = glob.glob('*.csv') #get all the csv file from the path
files.sort() #sort the file according to the name
# files.sort(key=os.path.getmtime) #one can also sort file according to modified time

# #plot method 1
# for i in range(0,len(files)):
#         data = pd.read_csv(files[i],sep=',',header=None) #read each csv file and copy to dataframe
#         data.columns=['temp_deg', 'temp_kel', 'resis', 'seeb', 'pf', 'lor_num', 'kappa_e'] #give columns names      
#         temperature = data.temp_deg
#         resistivity = data.resis*1e5
#         plt.scatter(temperature, resistivity, label=str(files[i].replace('.csv', ''))) #label each data set by the sample name, and sample name is the file name
# plt.legend()

#plot method 2
all_data=pd.DataFrame()
#merge all data into one dataframe, add column 'sample' referring to the sample name
for i in range(0,len(files)):
        data = pd.read_csv(files[i],sep=',',header=None) #read each csv file and copy to dataframe
        data.columns=['temp_deg', 'temp_kel', 'resis', 'seeb', 'pf', 'lor_num', 'kappa_e'] #give columns names
        data['sample']=str(files[i].replace('.csv', ''))  
        all_data=pd.concat([all_data,data])
        
grouped=all_data.groupby('sample')
for key, grp in all_data.groupby(['sample']):
    plt.scatter(grp['temp_deg'], grp['resis'], label=key)
plt.legend()

plt.show()

