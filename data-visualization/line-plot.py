import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import rcParams

#raw data from the paper 'Yifei Zhang, Advanced Electronic Materials, 2021'
#raw data is csv file without header.
#col0, 2theta value
#col1, intensity


##figure setting
#define font
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] 
rcParams['font.size']=12
my_color={'x = 0.00':'#e0f3db',
'x = 0.20':'#ccebc5',
'x = 0.23':'#a8ddb5',
'x = 0.25':'#7bccc4',
'x = 0.33':'#4eb3d3',
'x = 0.50':'#2b8cbe',
'x = 1.00':'#08589e'}


#set x, y labels
plt.xlabel('2\u03B8 (°)')
plt.ylabel('Intensity (a. u.)')
#hide y axis value
plt.yticks(fontsize=0)

##read raw data
path= ['./data/xrd_x=0.csv', 
       './data/xrd_x=0.25.csv', 
       './data/xrd_x=1.csv', 
       './data/xrd_standard.csv'
       ] #give the directory path for the raw data


#data clearning and join
all_data=pd.DataFrame()
#merge all data into one dataframe, add column 'sample' referring to the sample name
df1 = pd.read_csv(path[0],sep=',',header=None) #read each csv file and copy to dataframe
df1.columns=['2theta_deg', 'int'] #give columns names
df1['sample']='x = 0.00'
#nmax-min normalization
df1['norm_int'] = (df1.int-df1.int.min())/(df1.int.max()-df1.int.min())

df2 = pd.read_csv(path[1],sep=',',header=None) #read each csv file and copy to dataframe
df2.columns=['2theta_deg', 'int'] #give columns names
df2['sample']='x = 0.25'
#nmax-min normalization
df2['norm_int'] = (df2.int-df2.int.min())/(df2.int.max()-df2.int.min())+1

df3 = pd.read_csv(path[2],sep=',',header=None) #read each csv file and copy to dataframe
df3.columns=['2theta_deg', 'int'] #give columns names
df3['sample']='x = 1.00'
#max-min normalization
df3['norm_int'] = (df3.int-df3.int.min())/(df3.int.max()-df3.int.min())+2

#concat to one dataframe
all_data=pd.concat([df1, df2, df3])
        
grouped=all_data.groupby('sample')
for key, grp in all_data.groupby(['sample']):
    plt.plot(grp['2theta_deg'], grp['norm_int'], label=key, color=my_color[key])

#plot vertical line for standard sample
df4 = pd.read_csv(path[3],sep=',',header=None) #read each csv file and copy to dataframe
df4.columns=['2theta_deg', 'int', 'hkl'] #give columns names
df4['sample']='standard sample'
plt.vlines(df4['2theta_deg'], ymin=-0.2, ymax=0, color = 'black', label='standard')

plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1),ncol=4, frameon=False)

plt.savefig('./lineplot.pdf', bbox_inches='tight', dpi=300)


