import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import rcParams

#all data from the paper 'Yifei Zhang, Advanced Electronic Materials, 2021'



##figure setting
#define font
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] 
rcParams['font.size']=12

#set x, y labels
plt.xlabel('Temperature (°C)')
plt.ylabel('Resistivity (m$\Omega$cm)')


#3 csv data files for 3 measurements, in each file col[0] is temperature, col[1] is resistivity, col[2] is seebeck
df1 = pd.read_csv('./1.csv', sep=',', header=None)
df1.columns=['temp_deg', 'resis', 'seeb']
df1['sample']='first'
df2 = pd.read_csv('./2.csv', sep=',', header=None)
df2.columns=['temp_deg', 'resis', 'seeb']
df2['sample']='second'
df3 = pd.read_csv('./3.csv', sep=',', header=None)
df3.columns=['temp_deg', 'resis', 'seeb']
df3['sample']='third'
# sort the df3 in ascending 
df3.sort_values('temp_deg', inplace=True, ignore_index=True)

# create a new data file, calculate the mean value and standard deviation 
all_data=pd.concat([df1,df2,df3], axis=1)
all_data['temp_mean']=all_data[['temp_deg']].mean(axis=1, skipna=True)
all_data['temp_std']=all_data[['temp_deg']].std(axis=1, skipna=True)
all_data['res_mean']=all_data[['resis']].mean(axis=1, skipna=True)
all_data['res_std']=all_data[['resis']].std(axis=1, skipna=True)
all_data.to_csv('./statistics.csv', index=False)

df = pd.read_csv('./statistics.csv')

# errorbar plot for mean value and standard deviation
# plt.errorbar(x=df['temp_mean'], y=df['res_mean'], yerr=df['res_std'])

# shaded area plot for mean value and standard deviation
plt.plot(df['temp_mean'], df['res_mean'])
plt.fill_between(df['temp_mean'], df['res_mean']-df['res_std'], df['res_mean']+df['res_std'], alpha=0.5)


# plot each measurement
plt.scatter(df1['temp_deg'], df1['resis'], label='first')
plt.scatter(df2['temp_deg'], df2['resis'], label='second')
plt.scatter(df3['temp_deg'], df3['resis'], label='third')


plt.savefig('./average-multiple-measuremennts.png', bbox_inches='tight', dpi=300)

