# Introduction

This project is created by Yifei Zhang, [PhD from Chalmers University of Technology, Sweden](https://research.chalmers.se/en/person/yifeiz). Here is the contact information of Yifei Zhang: [Linkedin](http://linkedin.com/in/yifei-zhang-085366a9), [nbzhangyifei@gmail](mailto:nbzhangyifei@gmail). 

The project mainly uses Python for data analysis, including the cleaning raw data, data manipulation, visualization, statistics and modelling. As a PhD student, we need to create numurous visualizations to present our research results, such as publications, meeting with supervisors and fundings, workshops and conferences. Excel or Origin are good software to create figures as they are very easy to use, however, they are not so efficient when we need to plot 100 figures. Moreover, it is difficult to create consistent figures using Excel or Origin. At the beginning of my PhD study, I used origin for plotting figure. However, when I was writing my first manuscript I realized that it is not so efficient, because I needed to adjust each figure for so many times. Therefore, I choose **Python** for plotting. 

In this project, I will focus mainly on visualization, statistics and modelling of data. But you should know **data cleaning** is the most important, the cleaning process is shown in some scripts. 

# Data visualization

## Simple scatter plot
Create simple scatter plot using `simple-plot.py`. 

* import modules pandas, matplotlib for creating figures
```shell
import import matplotlib.pyplot as plt
import pandas as pd
```
* import the raw data
```shell
files=['./data/x = 0.00.csv', './data/x = 0.20.csv', './data/x = 0.23.csv', './data/x = 0.25.csv', './data/x = 0.33.csv', './data/x = 0.50.csv', './data/x = 1.00.csv']
```
* merge all the raw data to one pandas DataFrame
```shell
all_data=pd.DataFrame()

for i in range(0,len(files)):
        data = pd.read_csv(files[i],sep=',',header=None) #read each csv file and copy to dataframe
        data.columns=['temp_deg', 'temp_kel', 'resis', 'seeb', 'pf', 'lor_num', 'kappa_e'] #give columns names
        data['sample']=str(files[i].replace('.csv', ''))  
        all_data=pd.concat([all_data,data])
```
* we want to plot `resis` versus `temp_deg` fpr each `sample` (`resis` is y value, `temp_deg` is x value), we use groupby function
```shell
grouped=all_data.groupby('sample')
for key, grp in all_data.groupby(['sample']):
    plt.scatter(grp['temp_deg'], grp['resis'], label=key)
```
* ![simple-plot](./data-visualization/data/simple-plot.png/)

## Lineplot
Create lineplot using `line-plot.py`. It is very useful when creating spectrum at various conditions (e. g. temperatures, samples).

* import modules oandas, matplotlib, as well as rcParams for controling the font, color. import raw data, XRD data, col[0] is 2theta value, col[1] is intensity
```shell
path= ['./data/xrd_x=0.csv', 
       './data/xrd_x=0.25.csv', 
       './data/xrd_x=1.csv', 
       './data/xrd_standard.csv'
       ] 
```

* we need to do the data manipulation, normalize the intersity to the max-min, and then plot. 
```shell
df1 = pd.read_csv(path[0],sep=',',header=None) #read each csv file and copy to dataframe
df1.columns=['2theta_deg', 'int'] #give columns names
df1['sample']='x = 0.00'
#nmax-min normalization
df1['norm_int'] = (df1.int-df1.int.min())/(df1.int.max()-df1.int.min())
```

* we plot the data, and plot the XRD pattern for the standard sample, which is a vertical line plot
```shell
grouped=all_data.groupby('sample')
for key, grp in all_data.groupby(['sample']):
    plt.plot(grp['2theta_deg'], grp['norm_int'], label=key, color=my_color[key])

#plot vertical line for standard sample
df4 = pd.read_csv(path[3],sep=',',header=None) #read each csv file and copy to dataframe
df4.columns=['2theta_deg', 'int', 'hkl'] #give columns names
df4['sample']='standard sample'
plt.vlines(df4['2theta_deg'], ymin=-0.2, ymax=0, color = 'black', label='standard')
```

* ![lineplot](./data-visualization/data/lineplot.png/)

## Subplot
When creating figures for journals, in most cases each figure contains several subplot, so this script shows how to make subplot, and also with more control of the figure. 

* define the font, color. [ColorBtewer](https://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3) provides suitable color combination. 
```shell
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] 
rcParams['font.size']=12
my_color={'x = 0.00':'#e0f3db',
'x = 0.20':'#ccebc5',
'x = 0.23':'#a8ddb5',
'x = 0.25':'#7bccc4',
'x = 0.33':'#4eb3d3',
'x = 0.50':'#2b8cbe',
'x = 1.00':'#08589e'}
```
* define the figure and subplot, here it is two subplots in the one row, so `nrows = 1, ncols = 2`. `sharex = True` because temperature is the x-axis for both subplots. `ax1, ax2` refer to the two subplots, respectively. 
```shell
#subplot ax1 and ax2, they are within 1 row and 2 columns
fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharex=True, figsize=(6.4, 4), gridspec_kw={'wspace': 0.4})
#set x, y labels
ax1.set_xlabel('Temperature (°C)')
ax1.set_ylabel('Resistivity (m$\Omega$cm)')
ax2.set_xlabel('Temperature (°C)')
ax2.set_ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)')
ax2.set_ylim([-250, 10])
ax1.text(0.02,0.98,'(a)', transform=ax1.transAxes,
                            horizontalalignment='left',
                            verticalalignment='top') #add '(a)' in the top left corner of the first subplot ax1
ax2.text(0.02,0.98,'(b)', transform=ax2.transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')
```

* plot the data. Here, `ax1.scatter` means make a scatter in the `ax1` subplot, `color=my_color[key]` means that I want the color of each data set the same as what I define in the beginning. 
```shell
grouped=all_data.groupby('sample')
for key, grp in all_data.groupby(['sample']):
    ax1.scatter(grp['temp_deg'], grp['resis']*1e5, label=key, marker='o', color=my_color[key])
    ax2.scatter(grp['temp_deg'], grp['seeb']*1e6, label=key, marker='o', color=my_color[key])
```
![subplot](./data-visualization/data/subplot.png/)

## Average multiple measurements
Aveage multiple measurements, calculate the mean value and standard deviation, `average-multiple.measurement.py`

* I use `df.mean()` and `df.std()` to calculate the mean value and standard deviation
```shell
# create a new data file, calculate the mean value and standard deviation 
all_data=pd.concat([df1,df2,df3], axis=1)
all_data['temp_mean']=all_data[['temp_deg']].mean(axis=1, skipna=True)
all_data['temp_std']=all_data[['temp_deg']].std(axis=1, skipna=True)
all_data['res_mean']=all_data[['resis']].mean(axis=1, skipna=True)
all_data['res_std']=all_data[['resis']].std(axis=1, skipna=True)
````

* errorbar plot
```shell
# errorbar plot for mean value and standard deviation
plt.errorbar(x=df['temp_mean'], y=df['res_mean'], yerr=df['res_std'])
```
![errorbar](data-visualization/data/average-multiple-measuremennts.pdf)

* shaded area plot
```shell
# shaded area plot for mean value and standard deviation
plt.plot(df['temp_mean'], df['res_mean'])
plt.fill_between(df['temp_mean'], df['res_mean']-df['res_std'], df['res_mean']+df['res_std'], alpha=0.5)
```
![shaded-area](data-visualization/data/average-multiple-measuremennts.png/)

# Statistics
## Mean and standard deviation
## Gaussian distribution

# Data modelling - regression

# Machine learning
