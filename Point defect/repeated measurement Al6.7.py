import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
import csv


#col1, temperature (°C)
#col2, resistivity (Ohm*m)
#col3, Seebeck (V/K)
#col4, power factor (W/mK^2)


# # color=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
#               '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
#               '#bcbd22', '#17becf']
# color=['#e0f3db',
# '#ccebc5',
# '#a8ddb5',
# '#7bccc4',
# '#2b8cbe',
# '#2b8cbe',
# '#0868ac',
# '#084081']
# color=['#d1e5f0',
# '#67a9cf',
# '#2166ac',
# '#fee090',
# '#ef8a62',
# '#b2182b'
#        ]
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/repeated measurement/Al6.7'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))

file_list = []
for i in range(0,len(files)):
    df = pd.read_csv(files[i],header=None,usecols=[0],sep=',')
    file_list.append(df)
result = pd.concat(file_list,axis=1,ignore_index=True)   # 合并文件
result['mean']=result.mean(axis=1)
result['sd']=result.std(axis=1)
result.to_csv(path+'merge_tem.csv', index=False,header=None)  # 保存合并后的文件

file_list = []
for i in range(0,len(files)):
    df = pd.read_csv(files[i],header=None,usecols=[1],sep=',')
    file_list.append(df)
result = pd.concat(file_list,axis=1,ignore_index=True)   # 合并文件
result['mean']=result.mean(axis=1)
result['sd']=result.std(axis=1)
result.to_csv(path+'merge_res.csv', index=False,header=None)  # 保存合并后的文件

file_list = []
for i in range(0,len(files)):
    df = pd.read_csv(files[i],header=None,usecols=[2],sep=',')
    file_list.append(df)
result = pd.concat(file_list,axis=1,ignore_index=True)   # 合并文件
result['mean']=result.mean(axis=1)
result['sd']=result.std(axis=1)
result.to_csv(path+'merge_see.csv', index=False,header=None)  # 保存合并后的文件

fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((360,1000))
# plt.ylim((0,18))
new_xticks=np.linspace(400,1000, num=7)
##new_yticks=np.linspace(0.8,2, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=18)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)
plt.tick_params(direction='in', width=1,top=True, right=True)
plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=18)
plt.ylabel('Resistivity (m$\Omega$cm)', fontfamily=rcParams['font.family'], fontsize=18)


for i in range(0,len(files)):
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
      plt.scatter(temperature, resistivity, s=6**2,marker='s', edgecolor=None)

df2=pd.read_csv(path+'merge_tem.csv',header=None,usecols=[3])
df3=pd.read_csv(path+'merge_res.csv',header=None,usecols=[3])
df4=pd.read_csv(path+'merge_res.csv',header=None,usecols=[4])


tem=np.array(df2)+273.15
res=np.array(df3)*1e5
error=np.array(df4)*1e5

std=error/res
print(std)
plt.text(400,4,s='SD = 2-6 %',fontdict={'size':18, 'color':'black'})

# plt.errorbar(tem,res,yerr=error,color='black',linewidth=1)
# print(error/res)
plt.tight_layout()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/repeat/Al6.7_rho.pdf', format='pdf')

fig = plt.figure(num=2, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((360,1000))
# plt.ylim((0,18))
new_xticks=np.linspace(400,1000, num=7)
##new_yticks=np.linspace(0.8,2, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=18)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)
plt.tick_params(direction='in', width=1,top=True, right=True)
plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=18)
plt.ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=18)


for i in range(0,len(files)):
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      seebeck = df1.loc[:,2]*1e6  #transfer unit to mOhm*cm
      plt.scatter(temperature, seebeck, s=6**2,marker='s', edgecolor=None)

df2=pd.read_csv(path+'merge_tem.csv',header=None,usecols=[3])
df3=pd.read_csv(path+'merge_see.csv',header=None,usecols=[3])
df4=pd.read_csv(path+'merge_see.csv',header=None,usecols=[4])

tem=np.array(df2)+273.15
see=np.array(df3)*1e6
error=np.array(df4)*1e6

std=error/see
print(std)
plt.text(400,-200,s='SD = 0.4-3 %',fontdict={'size':18, 'color':'black'})

# plt.errorbar(tem,see,yerr=error,color='black',linewidth=1)
# print(error/res)
plt.tight_layout()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/repeat/Al6.7_see.pdf', format='pdf')


plt.show()
