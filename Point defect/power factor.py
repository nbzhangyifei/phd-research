import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.ticker import ScalarFormatter

#col1, temperature (°C)
#col2, temperature (K)
#col3, resistivity (Ohm*m)
#col4, Seebeck (V/K)
#col5, power factor (W/mK^2)
#col6, Lorenz number (1e-8*WOhm/K^2)
#col7, ke (W/mK)

# color=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
#               '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
#               '#bcbd22', '#17becf']

color=['#d1e5f0',
'#67a9cf',
'#2166ac',
'#ef8a62',
'#b2182b','#2166ac','#b2182b'
       ]
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/power factor'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)

plt.xlim((360,1000))
plt.ylim((0,2))
# plt.tick_params(direction='in', width=1,top=True, right=True)
ax.tick_params('x', which='both', direction='in', width=1,top=True, right=True)
ax.tick_params('y', which='both', direction='in', width=1,top=True, right=True)

for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
##plt.ticklabel_format(axis='both', style='plain')
new_xticks=np.linspace(400,1000, num=7)
# new_yticks=np.linspace(10,60, num=6)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=18)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)

# plt.tick_params(direction='in', width=1,top=True, right=True)

plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=18)
plt.ylabel('Power factor ($\mathregular{mWm^{-1}K^{-2}}$)', fontfamily=rcParams['font.family'], fontsize=18)


for i in [0,1,2,3,4]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      pf = df1.loc[:,3]*1000
      plt.scatter(temperature,pf, s=6**2,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))

for i in [5,6]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      pf = df1.loc[:,3]*1000
      plt.scatter(temperature,pf, s=6**2,marker='o', color='white', edgecolor=color[i], label=str(f.replace('.csv','')))

plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

##for i in [7,8]:
##      f=files[i]
##      data = pd.read_csv(f,header=None)
##      df1=pd.DataFrame(data)
##      temperature = df1.loc[:,1]
##      mobility = df1.loc[:,2]  
##      plt.scatter(temperature,mobility,s=6**2,marker='^',edgecolor=color[i], color = 'white', label=str(f.replace('.csv','')))
##      plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')


##x = np.linspace(600, 970, 20) ###alloy scattering
##y = 800*x**(-0.5)
##plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.7)
##plt.text(650, 35,'$\mathregular{T^{-1/2}}$',fontdict={'size':22, 'color':'black'})
# x = np.linspace(370,600, 20) ###fitting initial
# y = 6300*x**(-0.8)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='red')
####

# x = np.linspace(623,923, 20) ###mix scattering
# y = 4800*x**(-0.8)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(900,17,'$\mathregular{T^{-0.8}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(623,923, 20) ###mix scattering
# y = 700*x**(-0.5)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(700,18,'$\mathregular{T^{-0.55}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(623,923, 20) ###mix scattering
# y = 3600*x**(-0.75)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(900,17,'$\mathregular{T^{-0.75}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(573,773, 20) ###mix scattering
# y = 1900*x**(-0.65)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(700,30,'$\mathregular{T^{-0.65}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(623,923, 20) ###mix scattering
# y = 1850*x**(-0.65)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(920,22,'$\mathregular{T^{-0.65}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(623,873, 20) ###mix scattering
# y = 850*x**(-0.55)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(700,18,'$\mathregular{T^{-0.55}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(623,923, 20) ###mix scattering
# y = 580000*x**(-1.5)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(700,32,'$\mathregular{T^{-1.5}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(623,923, 20) ###mix scattering
# y = 600*x**(-0.5)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(700,18,'$\mathregular{T^{-0.5}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(373,573, 20) ###mix scattering
# y = 3700*x**(-0.75)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(400,42,'$\mathregular{T^{-0.75}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(373,623, 20) ###mix scattering
# y = 0.007*x**(1.24)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(400,10,'$\mathregular{T^{1.24}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(373,523, 20) ###mix scattering
# y = 1.3*x**(0.5)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(400,28,'$\mathregular{T^{0.5}}$',fontdict={'size':18, 'color':'black'})

# x = np.linspace(373,573, 20) ###mix scattering
# y = 0.3*x**(0.7)
# plt.plot(x,y, linestyle='dashed', linewidth='2', color='black', alpha=0.5)
# plt.text(400,16.5,'$\mathregular{T^{0.7}}$',fontdict={'size':18, 'color':'black'})



##for i in [9]:
##      f=files[i]
##      data = pd.read_csv(f,header=None)
##      df1=pd.DataFrame(data)
##      temperature = df1.loc[:,0]
##      mobility = df1.loc[:,3]
##      plt.scatter(temperature,mobility,s=8**2, marker='s',color='white',edgecolor='black', label=str(f.replace('.csv','')))
##      plt.tight_layout()
##      plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

plt.tight_layout()
plt.show()
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/power factor.pdf', format='pdf')



