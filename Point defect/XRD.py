import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob


color=['#d1e5f0',
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#b2182b'] 
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/XRD'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((24,60))
# plt.xlim((28,32))
# plt.ylim((-12,250))
# plt.ylim((-8,10))
##new_xticks=np.linspace(100,700, num=7)
##new_yticks=np.linspace(0.8,2, num=5)
plt.xticks(fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=0, color = 'white')
plt.tick_params(direction='in', width=1,top=True, right=True)
plt.xlabel('2\u03B8 (°)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Intensity (a. u.)', fontfamily=rcParams['font.family'], fontsize=22)


##for i in range(0,len(files)):
##      f=files[i]
##      data = pd.read_csv(f,header=None)
##      df1=pd.DataFrame(data)
##      theta = df1.loc[:,0]
##      inten = df1.loc[:,1]  
##      plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
##      plt.tight_layout()
##      plt.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
##fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/BGG+BAG_SPS/XRD.eps', format='eps')

for i in [0]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      theta = df1.loc[:,0]
##      inten = df1.loc[:,1]+10
      inten = df1.loc[:,1]
      plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
      # plt.tight_layout()
      # plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

for i in [1]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      theta = df1.loc[:,0]
##      inten = df1.loc[:,1]+20
      inten = df1.loc[:,1]-1.1
      plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
      # plt.tight_layout()
      # plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
      
for i in [2]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      theta = df1.loc[:,0]
##      inten = df1.loc[:,1]+30
      inten = df1.loc[:,1]-2.2
      plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
      # plt.tight_layout()
      # plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

for i in [3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      theta = df1.loc[:,0]
##      inten = df1.loc[:,1]+40
      inten = df1.loc[:,1]-3.3
      plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
      # plt.tight_layout()
      # plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
for i in [4]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      theta = df1.loc[:,0]
##      inten = df1.loc[:,1]+40
      inten = df1.loc[:,1]-4.4
      plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))

# for i in [5]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+40
#       inten = df1.loc[:,1]-5.5
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))

# for i in [6]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+40
#       inten = df1.loc[:,1]-6.6
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
      
      
for i in [6]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      theta = df1.loc[:,0]
      inten = df1.loc[:,1]
      plane = str(df1.loc[:,2])
      plt.vlines(x=theta, ymin=-5.2, ymax=-4.7,colors='k', label='$\mathregular{Ba_{8}Ga_{16}Ge_{30}}$ PDF 01-070-7540')


plt.annotate('Ge', xy = (27,-1), xytext = (24.5, -0.5), fontsize=22,arrowprops=dict(facecolor='black', width=1, headwidth=1.5, headlength=1.5))
# plt.text(17,0.3,'Al0.0',fontsize=22)
# plt.text(17,-0.8,'Al3.4',fontsize=22)
# plt.text(17,-1.9,'Al4.5',fontsize=22)
# plt.text(17,-3,'Al5.8',fontsize=22)
# plt.text(17,-4.1,'Al6.7',fontsize=22)
# plt.text(17,-5.5,'Al8.8')
# plt.text(17,-6,'PDF 01-070-7540')

plt.tight_layout()
# plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/PXRD.pdf', format='pdf')
      
# fig = plt.figure(num=2, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
# ax = fig.add_subplot(111)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)
# ##plt.xlim((20,60))
# plt.xlim((28,32))
# ##plt.ylim((-12,250))
# # plt.ylim((-1.5,20))
# ##new_xticks=np.linspace(100,700, num=7)
# ##new_yticks=np.linspace(0.8,2, num=5)
# plt.xticks(fontfamily=rcParams['font.family'], fontsize=18)
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=0, color = 'white')
# plt.tick_params(direction='in', width=1,top=True, right=True)
# plt.xlabel('2\u03B8 (°)', fontfamily=rcParams['font.family'], fontsize=22)
# plt.ylabel('Intensity (a. u.)', fontfamily=rcParams['font.family'], fontsize=22)


# ##for i in range(0,len(files)):
# ##      f=files[i]
# ##      data = pd.read_csv(f,header=None)
# ##      df1=pd.DataFrame(data)
# ##      theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]  
# ##      plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
# ##      plt.tight_layout()
# ##      plt.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# ##fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/BGG+BAG_SPS/XRD.eps', format='eps')

# for i in [0]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+10
#       inten = df1.loc[:,1]
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
#       # plt.tight_layout()
#       # plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

# for i in [1]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+20
#       inten = df1.loc[:,1]-1.1
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
#       # plt.tight_layout()
#       # plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
      
# for i in [2]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+30
#       inten = df1.loc[:,1]-2.2
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
#       # plt.tight_layout()
#       # plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

# for i in [3]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+40
#       inten = df1.loc[:,1]-3.3
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
#       # plt.tight_layout()
#       # plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# for i in [4]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+40
#       inten = df1.loc[:,1]-4.4
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))

# for i in [5]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+40
#       inten = df1.loc[:,1]-5.5
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))

# for i in [6]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
# ##      inten = df1.loc[:,1]+40
#       inten = df1.loc[:,1]-6.6
#       plt.plot(theta, inten, color=color[i], label=str(f.replace('.csv','')))
      
      
# # for i in [7]:
# #       f=files[i]
# #       data = pd.read_csv(f,header=None)
# #       df1=pd.DataFrame(data)
# #       theta = df1.loc[:,0]
# #       inten = df1.loc[:,1]
# #       plane = str(df1.loc[:,2])
# #       plt.vlines(x=theta, ymin=-0.6, ymax=-0.2,colors='k', label='$\mathregular{Ba_{8}Ga_{16}Ge_{30}}$ PDF 01-070-7540')

# ##for i in range(0, 30):
# ##  theta= df1.loc[i,0]
# ##  plane = str(df1.loc[i,2])
# ##  plt.text(x=theta-0.35, y=-8, s=plane,rotation=270, fontdict={'size':8, 'color':'black'})

# ##plt.annotate('Ge', xy = (27,15), xytext = (23, 90), arrowprops=dict(facecolor='black', width=0.5, headwidth=1.5, headlength=1.5))
# # plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# # plt.tight_layout()

# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/PXRD peak position.pdf', format='pdf')
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/BGG+BAG_SPS/afm/comment/XRD smaller_x=1.pdf', format='pdf')
plt.show()
