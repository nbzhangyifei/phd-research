import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.patches import Patch
from matplotlib.lines import Line2D


color=['#d1e5f0',
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#b2182b'] 

rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman']
rcParams['figure.figsize'] = (6.4, 3)
rcParams['font.size'] = 12
rcParams['figure.titlesize'] = 18
rcParams['legend.fontsize'] = 12
rcParams['savefig.dpi'] = 600
linewidth=1
labels = {'al00': 'Al0.0', 'al34': 'Al3.4', 'al45':'Al4.5', 'al58':'Al5.8', 'al67':'Al6.7'}


fig, (ax1,ax2) = plt.subplots(1, 2, gridspec_kw= {'wspace': 0.35})
xlim=[[24,60],[-0.3,16.3]]
xticks=[np.arange(30,61,10),np.arange(0,17,4)]


ax1.set_yticks([])
ax1.set_xticks(xticks[0])
ax1.set_xlim(xlim[0])

ax1.set_xlabel('2\u03B8 (°)')
ax1.set_ylabel('Intensity (a. u.)')
path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/XRD'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

for i in [0,1,2,3,4]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      theta = df1.loc[:,0]
      inten = df1.loc[:,1]-1.1*i
      ax1.plot(theta, inten, color=color[i], linewidth=1,label=None)
     
for i in [6]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      theta = df1.loc[:,0]
      inten = df1.loc[:,1]
      plane = str(df1.loc[:,2])
      ax1.vlines(x=theta, ymin=-5.2, ymax=-4.7,colors='k', linewidth=1,label=None)


ax1.annotate('Ge', xy = (27,-1), xytext = (24.5, -0.5),arrowprops=dict(facecolor='black', width=1, headwidth=1.5, headlength=1.5))


ax2.set_xticks(xticks[1])
ax2.set_xlim(xlim[1])

ax2.set_xlabel('Al content per unit cell')
ax2.set_ylabel('Lattice parameter (Å)')


f= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/unit cell as composition.csv'
data = pd.read_csv(f,header=None)
df=pd.DataFrame(data)


for i in range(0,5):
  comXRF=df.loc[i, 0]
  comXRD=df.loc[i,1]
  lat=df.loc[i, 2]
  error=df.loc[i,3]
  name=df.loc[i,4]
  ax2.scatter(comXRD,lat,marker='s',s=50,color=color[i],edgecolor=color[i], linewidth=1,label=name)
  
x1=[df.loc[0,1],16]
y1=[10.7802,10.815]

x2=[df.loc[0,1],16]
y2=[10.7828,10.85335]
##plt.plot(x1,y1,linestyle='solid', linewidth='1', color=None,alpha=0.5)
##plt.plot(x2,y2,linestyle='solid', linewidth='1', color='orange',alpha=0.5)
ax2.fill_between(x1,y1,y2,facecolor='orange',alpha=0.4)


f2= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/structure study/unit cell as composition reference.csv'
data2 = pd.read_csv(f2,header=None)
df2=pd.DataFrame(data2)
composition2 = df2.loc[:,0]
lp2 = df2.loc[:,3]#from Chr_JACS2006, n-type BaGaGe by Czochralski, single crystal XRD Mo Ka radiation
ax2.scatter(composition2,lp2,s=50, marker='s', color='white',edgecolor='black', linewidth=1,label=None)
lp2 = df2.loc[:,4]#from Chr_CM2007, BaAlGe by Czpchralski, single crystal XRD Mo Ka radiation
ax2.scatter(composition2,lp2,s=50, marker='s', color='white',edgecolor='black', linewidth=1,label=None)
# lp = df1.loc[:,7]#from AndRan:applied materials and interfaces_2018, BaAlGe by arc melting, powder XRD Mo Ka radiation
# plt.scatter(composition,lp,s=10**2, marker='s', color='white',edgecolor='black', label=None)


lp2 = df2.loc[:,5]#from Chr_CM2007, BaAlGe by normal shake and bake, single crystal XRD Mo Ka radiation
ax2.scatter(composition2,lp2,s=50, marker='s', color='white',edgecolor='black', linewidth=1,label=None)
lp2 = df2.loc[:,6]#from Chr_CM2007, BaAlGe by Al flux, single crystal XRD Mo Ka radiation
ax2.scatter(composition2,lp2,s=50, marker='s', color='white',edgecolor='black', linewidth=1,label=None)

f3= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/structure study/unit cell as composition.csv' #reference from other flux-grown samples
data3 = pd.read_csv(f3,header=None)
df3=pd.DataFrame(data3)

for i in [1,2,4]:
  comXRD3=df3.loc[i,1]
  lat3=df3.loc[i, 2]
  ax2.scatter(comXRD3,lat3,marker='s',s=50,color='white',edgecolor='black',linewidth=1, label=None)



# ax2.legend(loc='upper left',shadow=None,facecolor=None, edgecolor='black')

custom_lines = [Line2D([0], [0], color=color[0], lw=6),
                Line2D([0], [0], color=color[1], lw=6),
                Line2D([0], [0], color=color[2], lw=6),
                Line2D([0], [0], color=color[3], lw=6),
                Line2D([0], [0], color=color[4], lw=6)]

ax2.legend(custom_lines, ['Al0.0','Al3.4','Al4.5','Al5.8','Al6.7'],loc='lower center',bbox_to_anchor=(-0.2,1),ncol=5, frameon=False)

ax1.text(0.02,0.98,'(a)', transform=ax1.transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')

ax2.text(0.02,0.98,'(b)', transform=ax2.transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')

fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/RSC_JMCA/PXRD.pdf', format='pdf',bbox_inches='tight')

