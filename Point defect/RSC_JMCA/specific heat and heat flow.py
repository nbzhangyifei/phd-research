import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.lines import Line2D



color=[
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62'

       ]

rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman']
rcParams['figure.figsize'] = (3.2, 4)
rcParams['font.size'] = 12
rcParams['figure.titlesize'] = 18
rcParams['legend.fontsize'] = 12
rcParams['savefig.dpi'] = 600
linewidth=1

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/point defect/Cp and heat flow'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig, axs = plt.subplots(2, 1, sharex=True, gridspec_kw= {'hspace':0})
xlim=[[-0.3,7],[300,800]]
ylim=[[23,65],[0.295,0.52]]
xticks=[np.arange(0,8,1),np.arange(300,801,100)]
yticks=[np.arange(40,61,20),np.arange(0.3,0.46,0.05)]

axs[1].set_xticks(xticks[1])
axs[0].set_yticks(yticks[0])
axs[1].set_yticks(yticks[1])
axs[1].set_xlim(xlim[1])
axs[0].set_ylim(ylim[0])
axs[1].set_ylim(ylim[1])



axs[0].set_ylabel('Heat flow (mW)')
axs[1].set_ylabel('$C_{p}$ ($\mathregular{Jg^{-1}K^{-1}}$)')
axs[1].set_xlabel('$T$ (K)')






for i in range(0,len(files)):
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature1 =df1.loc[:,0]+273.15
      cp1 =df1.loc[:,1]
      temperature2 =df1.loc[:,2]+273.15
      hf =df1.loc[:,3]
      axs[0].plot(temperature2, hf,color=color[i],linewidth=1)
      axs[1].plot(temperature1, cp1,color=color[i],linewidth=1)

# ax1.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# plt.tight_layout()   
                                                     
# ax1.annotate(' ', xy = (519,45), xytext = (460,52), fontsize=12,arrowprops=dict(edgecolor='#2166ac', width=2, headwidth=4, headlength=5))
# ax1.annotate(' ', xy = (685,57), xytext = (670,61), fontsize=22,arrowprops=dict(edgecolor='#2166ac', width=2, headwidth=4, headlength=5))

# ax1.annotate(' ', xy = (700,51), xytext = (685,55), fontsize=22,arrowprops=dict(edgecolor='#67a9cf', width=2, headwidth=4, headlength=5))

# ax1.annotate(' ', xy = (685,43), xytext = (670,47), fontsize=22,arrowprops=dict(edgecolor='#ef8a62', facecolor='#ef8a62', width=2, headwidth=4, headlength=5))
# ax1.annotate(' ', xy = (615,37), xytext = (600,41), fontsize=22,arrowprops=dict(edgecolor='#fee090', facecolor='#fee090', width=2, headwidth=4, headlength=5))

# circle1 = plt.Circle((685, 50),radius=18,edgecolor='black',facecolor='w',linewidth=2,alpha=0.7)
# ax1.add_patch(circle1)
# circle2 = plt.Circle((610, 35),radius=8,edgecolor='black',facecolor='w',linewidth=2,alpha=0.7)
# ax1.add_patch(circle2)
# ax1.annotate('Vacancy-induced transition', xy = (519,45), xytext = (460,52), fontsize=12,arrowprops=dict(edgecolor='#2166ac', width=2, headwidth=4, headlength=5))
axs[0].text(320,50,'vacancy-induced')
axs[0].text(550,25,'order-disorder')

axs[0].fill_betweenx(y=[23,65], x1=495, x2=520,color='grey',linewidth=0,alpha=0.5)
axs[1].fill_betweenx(y=[0.295,0.52], x1=495, x2=520,color='grey',linewidth=0,alpha=0.5)

axs[0].fill_betweenx(y=[23,65], x1=550, x2=700,color='grey',linewidth=0,alpha=0.2)
axs[1].fill_betweenx(y=[0.295,0.52], x1=550, x2=700,color='grey',linewidth=0,alpha=0.2)

axs[0].text(0.02,0.98,'(a)', transform=axs[0].transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')

axs[1].text(0.02,0.98,'(b)', transform=axs[1].transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')

custom_lines = [Line2D([0], [0],color=color[0], lw=1),
                Line2D([0], [0], color=color[1], lw=1),
                Line2D([0], [0],color=color[2], lw=1),
                Line2D([0], [0], color=color[3], lw=1),
]
axs[0].legend(custom_lines, ['Al3.4','Al4.5','Al5.8','Al6.7'],loc='lower center',bbox_to_anchor=(0.5,1),ncol=2,frameon=False)

fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/point defect/RSC_JMCA/Cp and heat flow.pdf', bbox_inches='tight',format='pdf')

