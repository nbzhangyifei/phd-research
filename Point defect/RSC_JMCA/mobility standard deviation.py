import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.ticker import ScalarFormatter
from matplotlib.lines import Line2D
#col1, temperature (°C)
#col2, temperature (K)
#col3, resistivity (Ohm*m)
#col4, Seebeck (V/K)
#col5, power factor (W/mK^2)
#col6, Lorenz number (1e-8*WOhm/K^2)
#col7, ke (W/mK)

# color=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
#               '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
#               '#bcbd22', '#17becf']

color=[

'#2166ac',

'#ef8a62'

       ]
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman']
rcParams['figure.figsize'] = (3.2, 3)
rcParams['font.size'] = 12
rcParams['figure.titlesize'] = 18
rcParams['legend.fontsize'] = 12
rcParams['savefig.dpi'] = 600
linewidth=1

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/mobility standard deviation'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig, axs= plt.subplots(1,1)
xlim=[[-0.3,7],[360,800],[360,800],[360,800]]
ylim=[[-1.8,2],[10,58],[0,19],[20,50]]
xticks=[np.arange(0,8,1),np.arange(400,901,100),np.arange(400,1001,100),np.arange(400,801,100)]
yticks=[np.arange(-1.5,1.6,0.5),np.arange(10,51,10),np.arange(0,17.6,2.5), np.arange(20,51,10)]

axs.set_xticks(xticks[3])
axs.set_yticks(yticks[3])
axs.set_xlim(xlim[3])
axs.set_ylim(ylim[3])
axs.set_xscale('log')
axs.set_yscale('log')
axs.xaxis.set_major_formatter(ScalarFormatter(useMathText=True))
axs.xaxis.set_minor_formatter(ScalarFormatter(useMathText=True))
axs.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
axs.yaxis.set_minor_formatter(ScalarFormatter(useMathText=True))
axs.set_xlabel('$T$ (K)')
axs.set_ylabel('$\mu_{w}$ ($\mathregular{cm^{2}V^{-1}s^{-1}}$)')



f=files[2]
data = pd.read_csv(f,header=None)
df1=pd.DataFrame(data)
temperature = df1.loc[:,0]
mobility = df1.loc[:,1]
axs.scatter(temperature,mobility, marker='s',s=50,color='white', edgecolor='#2166ac', label='Al4.5 annealed')

f=files[0]
data = pd.read_csv(f,header=None)
df1=pd.DataFrame(data)
temperature = df1.loc[:,0]
mobility = df1.loc[:,1]
error=df1.loc[:,2]
axs.errorbar(temperature,mobility, yerr=error, fmt='s',ms=8,color=color[0], edgecolor=None, label='Al4.5')

f2 = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/Al4.5 anneal/Al5.8.csv'

data2 = pd.read_csv(f2,header=None)
df2=pd.DataFrame(data2)
temperature2 = df2.loc[:8,0]+273.15
mobility2 = df2.loc[:8,8]
axs.scatter(temperature2,mobility2,marker='s',s=50,color='#fee090', edgecolor=None, label='Al5.8')


f=files[1]
data = pd.read_csv(f,header=None)
df1=pd.DataFrame(data)
temperature = df1.loc[:8,0]
mobility = df1.loc[:8,1]
error=df1.loc[:8,2]
axs.errorbar(temperature,mobility, yerr=error, fmt='s',ms=8,color=color[1], edgecolor=None, label='Al6.7')

custom_lines = [Line2D([0], [0], marker='s',color='#2166ac',lw=0),
                Line2D([0], [0], marker='s',color='#fee090',lw=0),
                Line2D([0], [0], marker='s',color=color[1],lw=0),
                Line2D([0], [0], marker='s',color='w',markeredgecolor='#2166ac',lw=0)
]
axs.legend(custom_lines, ['Al4.5','Al5.8','Al6.7','Al4.5_anneal'],loc='lower center',bbox_to_anchor=(0.5,1),ncol=2,frameon=False)

      



fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/RSC_JMCA/mobility standard deviation.pdf', bbox_inches='tight',format='pdf')



