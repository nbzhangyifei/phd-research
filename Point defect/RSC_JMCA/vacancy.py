#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 15:55:11 2020

@author: yifeiz
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.lines import Line2D
color=[

'#2166ac',
'#fee090',
'#ef8a62',
'#2166ac','#ef8a62'
        ]
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman']
rcParams['figure.figsize'] = (3.2, 4)
rcParams['font.size'] = 12
rcParams['figure.titlesize'] = 18
rcParams['legend.fontsize'] = 12
rcParams['savefig.dpi'] = 600
linewidth=1

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/Al4.5 anneal'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig, axs = plt.subplots(2, 1, sharex=True, gridspec_kw= {'hspace':0})
xlim=[[-0.3,7],[360,1000]]
ylim=[[1,12],[-290,-60]]
xticks=[np.arange(0,8,1),np.arange(400,1001,100)]
yticks=[np.arange(0,11,2.5),np.arange(-250,-99,50)]

axs[1].set_xticks(xticks[1])
axs[0].set_yticks(yticks[0])
axs[1].set_yticks(yticks[1])
axs[1].set_xlim(xlim[1])
axs[0].set_ylim(ylim[0])
axs[1].set_ylim(ylim[1])





axs[0].set_ylabel('$\u03C1$ (m$\Omega$cm)')
axs[1].set_ylabel('$S$ ($\mathregular{\u03BCVK^{-1}}$)')
axs[1].set_xlabel('$T$ (K)')

for i in [0,1,2]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      rho = df1.loc[:,1]*1e5
      see=df1.loc[:,2]*1e6
      uw = df1.loc[:,8]
      axs[0].scatter(temperature, rho,s=50,marker='s',color=color[i],edgecolor=None)
      # ax2.scatter(temperature, see,s=50,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
      axs[1].scatter(temperature, see,s=50,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

for i in [3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      rho = df1.loc[:,1]*1e5
      see=df1.loc[:,2]*1e6
      uw = df1.loc[:,8]
      axs[0].scatter(temperature, rho,s=50,marker='s',color='w',edgecolor='#2166ac')
      # ax2.scatter(temperature, see,s=50,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
      axs[1].scatter(temperature, see,s=50,marker='s',color='w',edgecolor='#2166ac', label='Al4.5 annealed')
      
axs[0].fill_betweenx(y=[1,12], x1=495, x2=520,color='grey',linewidth=0,alpha=0.5)
axs[0].text(400,7.5,'vacancy-induced')

custom_lines = [Line2D([0], [0], marker='s',color=color[0],lw=0),
                Line2D([0], [0], marker='s',color=color[1],lw=0),
                Line2D([0], [0], marker='s',color=color[2],lw=0),
                Line2D([0], [0], marker='s',color='w',markeredgecolor=color[0],lw=0)
]
axs[0].legend(custom_lines, ['Al4.5','Al5.8','Al6.7','Al4.5_anneal'],loc='lower center',bbox_to_anchor=(0.5,1),ncol=2,frameon=False)

fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/RSC_JMCA/vacancy.pdf',bbox_inches='tight', format='pdf')


plt.show()