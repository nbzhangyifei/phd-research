#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 15:55:11 2020

@author: yifeiz
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.ticker import ScalarFormatter
color=[
'black',
'#ef8a62',
'#b2182b',
'#b2182b'
        ]
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/comparison with modulation'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig, (ax1, ax2) = plt.subplots(2, dpi=dpi,sharex=True, gridspec_kw={'hspace': 0}, figsize=(6.4,7.2))
# fig, (ax1, ax2,ax3) = plt.subplots(3, dpi=dpi,sharex=True, gridspec_kw={'hspace': 0}, figsize=(6.4,6.4))
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(1.5)
  ax2.spines[axis].set_linewidth(1.5)
  # ax3.spines[axis].set_linewidth(1.5)

new_xticks=np.linspace(400,900, num=6)
# new_yticks=np.linspace(0.5,2.6, num=4)

ax1.axis(xmin=360, xmax=950)
ax1.tick_params(direction='in', width=1,top=True, right=True)
ax2.tick_params(direction='in', width=1,top=True, right=True)
# ax3.tick_params(direction='in', width=1,top=True, right=True)

ax1.tick_params(axis='y', labelsize=22)
ax2.tick_params(axis='y', labelsize=22)
# ax3.tick_params(axis='y', labelsize=22)

# ax3.tick_params(axis='x', labelsize=22)
# ax3.set_xticks(new_xticks)
ax2.tick_params(axis='x', labelsize=22)
ax2.set_xticks(new_xticks)
# ax1.set_yticks(new_yticks)

ax1.set_ylabel('Resistivity (m$\Omega$cm)', fontfamily=rcParams['font.family'], fontsize=22)
ax2.set_ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
# ax3.set_ylabel('Weighted mobility ($\mathregular{cm^{2}V^{-1}s^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
# ax3.set_xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
ax2.set_xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)

for i in [0,1,2]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]
      rho = df1.loc[:,1]*1e5
      see=df1.loc[:,2]*1e6
      ax1.scatter(temperature,rho,marker='s',s=100,color=color[i],edgecolor=None)
      ax2.scatter(temperature,see,marker='s',s=100,color=color[i],edgecolor=None,label=str(f.replace('.csv','')))
      uw = df1.loc[:,3]
      # ax3.scatter(temperature, uw,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# ax3.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
ax2.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/comparison with modulation_tran.pdf', format='pdf')

fig = plt.figure(num=2, dpi=dpi, figsize=(6.4, 7.2) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
ax = plt.axes(xscale='log', yscale='log')
plt.xlim((360,950))
# plt.ylim((9,50))
# plt.tick_params(direction='in', width=1,top=True, right=True)
ax.tick_params('x', which='both', direction='in', width=1,top=True, right=True)
ax.tick_params('y', which='both', direction='in', width=1,top=True, right=True)
ax.xaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
##plt.ticklabel_format(axis='both', style='plain')
new_xticks=np.linspace(400,900, num=6)
new_yticks=np.linspace(10,60, num=6)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(ticks=new_yticks,fontfamily=rcParams['font.family'], fontsize=22)

# plt.tick_params(direction='in', width=1,top=True, right=True)

plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Weighted mobility ($\mathregular{cm^{2}V^{-1}s^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)


for i in [0,1,2]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]
      uw = df1.loc[:,3]
      plt.scatter(temperature, uw,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/comparison with modulation_mobility.pdf', format='pdf')

plt.show()