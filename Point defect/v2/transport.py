import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob


#col1, temperature (°C)
#col2, resistivity (Ohm*m)
#col3, Seebeck (V/K)
#col4, power factor (W/mK^2)


# # color=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
#               '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
#               '#bcbd22', '#17becf']
# color=['#e0f3db',
# '#ccebc5',
# '#a8ddb5',
# '#7bccc4',
# '#2b8cbe',
# '#2b8cbe',
# '#0868ac',
# '#084081']
color=['#d1e5f0',
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#b2182b'
       ] #for Al0.0, 3.4, 4.5, 5.8, 6.7, 8.8
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/transport'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((360,1000))
plt.ylim((0,19))
new_xticks=np.linspace(400,1000, num=7)
##new_yticks=np.linspace(0.8,2, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)
plt.tick_params(direction='in', width=1,top=True, right=True)
plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Resistivity (m$\Omega$cm)', fontfamily=rcParams['font.family'], fontsize=22)


for i in range(0,len(files)):
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
      plt.scatter(temperature, resistivity, s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

plt.tight_layout()
plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/figures for submission/v2/raw/resistivity.pdf', format='pdf')

fig = plt.figure(num=2, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((360,1000))
new_xticks=np.linspace(400,1000, num=7)
##new_yticks=np.linspace(0.8,2, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)
plt.tick_params(direction='in', width=1,top=True, right=True)
plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
for i in range(0,len(files)):
      f=files[i]
      data = pd.read_csv(f, header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      seebeck = df1.loc[:,2]*1e6 #transfer unit to uV/K
      plt.scatter(temperature, seebeck, s=100, marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/figures for submission/v2/raw/seebeck.pdf', format='pdf')

# fig = plt.figure(num=3, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
# ax = fig.add_subplot(111)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)
# # plt.xlim((1.2,2.7))
# ##plt.ylim((0,10))
# # new_xticks=np.linspace(400,1000, num=7)
# ##new_yticks=np.linspace(0.8,2, num=5)
# plt.xticks(fontfamily=rcParams['font.family'], fontsize=18)
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)
# plt.tick_params(direction='in', width=1,top=True, right=True)
# plt.xlabel('1000/T ($\mathregular{K^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=18)
# plt.ylabel('Conductivity (S/cm)', fontfamily=rcParams['font.family'], fontsize=18)


# for i in range(0,3):
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature =(df1.loc[:,0]+273.15)
#       resistivity = 0.001/df1.loc[:,1]  #transfer unit to mOhm*cm
#       plt.scatter(temperature, resistivity, s=6**2,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# plt.tight_layout()
# plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# plt.tight_layout()
# # fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/conductivity.pdf', format='pdf')

# x1=np.linspace(350, 900, 50)
# y1=0.005*x1**(1.25)
# plt.plot(x1,y1)

plt.show()


##fig = plt.figure(num=3, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
##ax = fig.add_subplot(111)
##for axis in ['top','bottom','left','right']:
##  ax.spines[axis].set_linewidth(1.5)
##plt.xlim((0,800))
##new_xticks=np.linspace(100,700, num=7)
####new_yticks=np.linspace(0.8,2, num=5)
##plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=14)
##plt.yticks(fontfamily=rcParams['font.family'], fontsize=14)
##plt.tick_params(direction='in', width=1,top=True, right=True)
##plt.xlabel('Temperature (°C)', fontfamily=rcParams['font.family'], fontsize=22)
##plt.ylabel('Power factor (mW/mK\u00b2)', fontfamily=rcParams['font.family'], fontsize=22)
##for i in range(0,len(files)):
##      f=files[i]
##      data = pd.read_csv(f, header=None)
##      df1=pd.DataFrame(data)
##      temperature = df1.loc[:,0]
##      pf = df1.loc[:,3]*1e3 #transfer unit to mW/mK

##      plt.scatter(temperature, pf, s=4**2, marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
##      plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black', )
##fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/BAGG_flux_SPS/power factor_all.eps', format='eps')


##plt.show()
