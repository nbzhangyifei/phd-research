#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 29 13:03:05 2020

@author: yifeiz
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.ticker import ScalarFormatter

#col1, temperature (°C)
#col2, temperature (K)
#col3, resistivity (Ohm*m)
#col4, Seebeck (V/K)
#col5, power factor (W/mK^2)
#col6, Lorenz number (1e-8*WOhm/K^2)
#col7, ke (W/mK)

# color=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
#               '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
#               '#bcbd22', '#17becf']

color=['#d1e5f0',
'#67a9cf',
'#2166ac',
'#ef8a62',
'#b2182b'
       ]
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

file = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/scattering mechanism.csv'
# path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/mobility'
# extension = 'csv'
# os.chdir(path)
# files = glob.glob('*.{}'.format(extension))
# files.sort(key=os.path.getmtime) #sort file according to modified time
fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((-0.3,9))
plt.ylim((-1.8,2.4))
new_xticks=np.linspace(0,7, num=8)
new_yticks=np.linspace(-1.5,1.5, num=7)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)
plt.tick_params(direction='in', width=1,top=True, right=True)
plt.xlabel('x in $\mathregular{Ba_{8}Al_{x}Ga_{16-x}Ge_{30}}$', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Exponential', fontfamily=rcParams['font.family'], fontsize=22)

data = pd.read_csv(file,header=None)
df=pd.DataFrame(data)

com=df.loc[0:5, 0]
lowT=df.loc[0:5,1]
highT=df.loc[0:5,2]
plt.plot(com,lowT,marker='s',ms= 10,color='blue', label='Lower temperature range')
plt.plot(com,highT,marker='s',ms= 10,color='red', label='Higher temperature range')

x1=[-0.3, 9]
y1=[1.5,1.5]
plt.plot(x1,y1,linestyle='--', color='blue')
y2=[-0.5, -0.5]
plt.plot(x1,y2,linestyle='--', color='red')
y3=[-1.5,-1.5]
plt.plot(x1,y3,linestyle='--', color='black')

plt.text(4.3,1.2,'Ionization scattering',fontdict={'size':22, 'color':'blue'})
plt.text(0,-0.4,'Alloy scattering',fontdict={'size':22, 'color':'red'})
plt.text(2.7,-1.4,'Acoustic phonon scattering',fontdict={'size':22, 'color':'black'})

# x2=[0,4.5]
# y4=[-1.8,-1.8]
# y5=[2.4,2.4]
# plt.fill_between(x2, y4,y5, facecolor='grey',alpha=0.4
                 # )
plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

plt.tight_layout()
plt.show()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/figures for submission/v2/raw/scattering mechanism.pdf', format='pdf')