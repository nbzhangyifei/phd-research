import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob

##col0, Al from XRF
##col1, Al from XRD
##col2, lattice parameter, XRD 300 K
##col3, error
##col4, sample name



# color=['b', 'g','c', 'm', 'y', 'k'] 
# color=['#ccebc5','#a8ddb5','#7bccc4','#4eb3d3','#2b8cbe','#2b8cbe','#08589e']

color=[

'#d1e5f0',
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#b2182b']

# color=['#d1e5f0',
# '#67a9cf',
# '#2166ac',
# '#ef8a62',
# '#f46d43',
# '#b2182b',
# '#7f0000',
# '#800026'
# ]
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

f= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/unit cell as composition.csv'

##fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
##ax = fig.add_subplot(111)
##for axis in ['top','bottom','left','right']:
##  ax.spines[axis].set_linewidth(1.5)
##plt.xlim((-0.3,16.3))
####plt.ylim((1,3))
##new_xticks=np.linspace(0,16, num=9)
####new_yticks=np.linspace(1,3, num=5)
##plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=18)
####plt.yticks(ticks=new_yticks,fontfamily=rcParams['font.family'], fontsize=18)
####plt.xticks(fontfamily=rcParams['font.family'], fontsize=18)
##plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)
##plt.tick_params(direction='in', width=1,top=True, right=True)
##
##plt.xlabel('x in Ba$_8$Al$_x$Ga$_1$$_6$$_-$$_x$Ge$_3$$_0$', fontfamily=rcParams['font.family'], fontsize=22)
##plt.ylabel('Lattice parameter (Å)', fontfamily=rcParams['font.family'], fontsize=22)
##
##data = pd.read_csv(f,header=None)
##df=pd.DataFrame(data)
##for i in range(0, 4):
##  comXRF=df.loc[i, 0]
##  comXRD=df.loc[i,1]
##  lat=df.loc[i, 2]
##  error=df.loc[i,3]
##  name=df.loc[i,4]
##  plt.errorbar(comXRF,lat,yerr=error,fmt='s',ms=6,color='red',edgecolor=None, label=name)
##
##comXRF=df.loc[5, 0]
##comXRD=df.loc[5,1]
##lat=df.loc[5, 2]
##error=df.loc[5,3]
##name=df.loc[5,4]
##plt.errorbar(comXRF,lat,yerr=error,fmt='s',ms=6,color='blue',edgecolor=None, label=name)

##f2= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/structure study/unit cell as composition reference.csv'
##data = pd.read_csv(f2,header=None)
##df1=pd.DataFrame(data)
##composition = df1.loc[:,0]
####lp = df1.loc[:,1]
####plt.scatter(composition,lp,s=6**2, marker='s', color='red',edgecolor=None, label='this work, SD')
##lp = df1.loc[:,2]
##plt.scatter(composition,lp,s=6**2, marker='^', color='black',edgecolor=None, label='zone melting')
##lp = df1.loc[:,3]
##plt.scatter(composition,lp,s=6**2, marker='s', color='black',edgecolor=None, label='Czochralski')
##lp = df1.loc[:,4]
##plt.scatter(composition,lp,s=6**2, marker='s', color='black',edgecolor=None, label=None)
##lp = df1.loc[:,5]
##plt.scatter(composition,lp,s=6**2, marker='o', color='black',edgecolor=None, label='melting reaction')
##lp = df1.loc[:,6]
##plt.scatter(composition,lp,s=6**2, marker='v', color='black',edgecolor=None, label='flux growth')
##lp = df1.loc[:,7]
##plt.scatter(composition,lp,s=6**2, marker='o', color='black',edgecolor=None, label=None)
##lp = df1.loc[:,8]
##plt.scatter(composition,lp,s=6**2, marker='o', color='black',edgecolor=None, label=None)
##
##plt.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
##plt.tight_layout()
##
##x1=[0,16]
##y1=[10.7828, 10.83863]
##plt.plot(x1,y1,color='black', linewidth='2')

fig = plt.figure(num=2, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((-0.3,16.3))
##plt.ylim((1,3))
new_xticks=np.linspace(0,16, num=9)
##new_yticks=np.linspace(1,3, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=18)
##plt.yticks(ticks=new_yticks,fontfamily=rcParams['font.family'], fontsize=18)
##plt.xticks(fontfamily=rcParams['font.family'], fontsize=18)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)
plt.tick_params(direction='in', width=1,top=True, right=True)

plt.xlabel('x in $\mathregular{Ba_{8}Al_{x}Ga_{16-x}Ge_{30}}$', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Lattice parameter (Å)', fontfamily=rcParams['font.family'], fontsize=22)

data = pd.read_csv(f,header=None)
df=pd.DataFrame(data)


for i in range(0,5):
  comXRF=df.loc[i, 0]
  comXRD=df.loc[i,1]
  lat=df.loc[i, 2]
  error=df.loc[i,3]
  name=df.loc[i,4]
  plt.scatter(comXRD,lat,marker='s',s=100,color=color[i],edgecolor=None, label=name)

# comXRF=df.loc[0:3, 0]
# comXRD=df.loc[0:3,1]
# lat=df.loc[0:3, 2]
# error=df.loc[0:3,3]
# name=df.loc[0:3,4]
# plt.errorbar(comXRD,lat,yerr=error,fmt='s',ms=10,color='red', label='vacancies')

# comXRF=df.loc[4:7, 0]
# comXRD=df.loc[4:7,1]
# lat=df.loc[4:7, 2]
# error=df.loc[4:7,3]
# name=df.loc[4:7,4]
# plt.errorbar(comXRD,lat,yerr=error,fmt='s',ms=10,color='blue',label='F-Al samples')


##comXRF=df.loc[6, 0]
##comXRD=df.loc[6,1]
##lat=df.loc[6, 2]
##error=df.loc[6,3]
##name=df.loc[6,4]
##plt.errorbar(comXRD,lat,yerr=error,fmt='s',ms=6,color='red',edgecolor=None, label='F-Al samples')

# comXRF=df.loc[9, 0]
# comXRD=df.loc[9,1]
# lat=df.loc[9, 2]
# error=df.loc[9,3]
# name=df.loc[9,4]
# plt.errorbar(comXRD,lat,yerr=error,fmt='s',ms=6,color='blue',edgecolor=None, label='C-Al5.2')









x1=[df.loc[0,1],16]
y1=[10.7802,10.819]

x2=[df.loc[0,1],16]
y2=[10.7828,10.85335]
##plt.plot(x1,y1,linestyle='solid', linewidth='1', color=None,alpha=0.5)
##plt.plot(x2,y2,linestyle='solid', linewidth='1', color='orange',alpha=0.5)
plt.fill_between(x1,y1,y2,facecolor='orange',alpha=0.4)


f2= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/structure study/unit cell as composition reference.csv'
data2 = pd.read_csv(f2,header=None)
df2=pd.DataFrame(data2)
composition2 = df2.loc[:,0]
# lp = df1.loc[:,1]
# plt.scatter(composition,lp,s=6**2, marker='s', color='red',edgecolor=None, label='this work, SD')
# lp = df1.loc[:,2]#from Chr_JACS2006, p-type BaGaGe by Ga flux, single crystal XRD Mo Ka radiation
# plt.scatter(composition,lp,s=100, marker='s', color='white',edgecolor='black', label=None)
lp2 = df2.loc[:,3]#from Chr_JACS2006, n-type BaGaGe by Czochralski, single crystal XRD Mo Ka radiation
plt.scatter(composition2,lp2,s=10**2, marker='s', color='white',edgecolor='black', label=None)
lp2 = df2.loc[:,4]#from Chr_CM2007, BaAlGe by Czpchralski, single crystal XRD Mo Ka radiation
plt.scatter(composition2,lp2,s=10**2, marker='s', color='white',edgecolor='black', label=None)
# lp = df1.loc[:,7]#from AndRan:applied materials and interfaces_2018, BaAlGe by arc melting, powder XRD Mo Ka radiation
# plt.scatter(composition,lp,s=10**2, marker='s', color='white',edgecolor='black', label=None)


lp2 = df2.loc[:,5]#from Chr_CM2007, BaAlGe by normal shake and bake, single crystal XRD Mo Ka radiation
plt.scatter(composition2,lp2,s=10**2, marker='s', color='white',edgecolor='black', label=None)
lp2 = df2.loc[:,6]#from Chr_CM2007, BaAlGe by Al flux, single crystal XRD Mo Ka radiation
plt.scatter(composition2,lp2,s=10**2, marker='s', color='white',edgecolor='black', label=None)

f3= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/structure study/unit cell as composition.csv' #reference from other flux-grown samples
data3 = pd.read_csv(f3,header=None)
df3=pd.DataFrame(data3)

for i in [1,2,4,5]:
  comXRD3=df3.loc[i,1]
  lat3=df3.loc[i, 2]
  plt.scatter(comXRD3,lat3,marker='s',s=10**2,color='white',edgecolor='black', label=None)

##x = np.linspace(0, 16, 20)
##y = 0.0032*x+10.78024
##plt.plot(x,y, linestyle='dashed', linewidth='2', color='black')

##x = np.linspace(0, 16, 20)
##y = 0.00321*x+10.77851
##plt.plot(x,y, linestyle='dashed', linewidth='2', color='black')

plt.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()
plt.show()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/unit cell as composition.pdf', format='pdf')





