#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 15:55:11 2020

@author: yifeiz
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.lines import Line2D
color=[
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#2166ac','#ef8a62'
        ]
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman']
rcParams['figure.figsize'] = (3.4, 8.2)
rcParams['font.size'] = 12
rcParams['figure.titlesize'] = 18
rcParams['legend.fontsize'] = 12
rcParams['savefig.dpi'] = 300
linewidth=1


fig, axs = plt.subplots(3, 1, sharex=True, gridspec_kw= {'hspace':0})
xlim=[[-0.3,2],[350,1000]]
ylim=[[0.6,2.7],[0,1.2],[0.01,0.75]]
xticks=[np.arange(0,8,1),np.arange(400,1001,100)]
yticks=[np.arange(0.75,2.6,0.25),np.arange(0.25,1.1,0.25),np.arange(0,0.91,0.1)]

axs[2].set_xticks(xticks[1])
axs[0].set_yticks(yticks[0])
axs[1].set_yticks(yticks[1])
axs[2].set_yticks(yticks[2])
axs[2].set_xlim(xlim[1])
axs[0].set_ylim(ylim[0])
axs[1].set_ylim(ylim[1])
axs[2].set_ylim(ylim[2])




path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/k and zt'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time



axs[0].set_ylabel('Thermal conductivity ($\mathregular{Wm^{-1}K^{-1}}$)')
axs[1].set_ylabel('Power factor ($\mathregular{mWm^{-1}K^{-2}}$)')
axs[2].set_ylabel('Figure of merit')
axs[2].set_xlabel('Temperature (K)')

for i in [0,1,2,3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      k = df1.loc[:,6]
      error=df1.loc[:,7]
      axs[0].errorbar(temperature,k,yerr=error,fmt='s',ms=8,lw=1,color=color[i],edgecolor=None)
      kl = df1.loc[:,8]
      zt=df1.loc[:,9]
      axs[2].scatter(temperature, zt,s=50,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# ax3.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

path2 = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/transport'
extension = 'csv'
os.chdir(path2)
files2 = glob.glob('*.{}'.format(extension))
files2.sort(key=os.path.getmtime)

for i in [1,2,3,4]:
      f2=files2[i]
      data2 = pd.read_csv(f2, header=None)
      df2=pd.DataFrame(data2)
      temperature2 = df2.loc[:,0]+273.15
      pf2 = df2.loc[:,3]*1e3 #transfer unit to mW/mK
      axs[1].scatter(temperature2, pf2, s=50, marker='s',color=color[i-1],edgecolor=None, label=str(f.replace('.csv','')))
# ax2.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

custom_lines = [Line2D([0], [0], marker='s',color=color[0],lw=0),
                Line2D([0], [0], marker='s',color=color[1],lw=0),
                Line2D([0], [0], marker='s',color=color[2],lw=0),
                Line2D([0], [0], marker='s',color=color[3],lw=0)
]
axs[0].legend(custom_lines, ['Al3.4','Al4.5','Al5.8','Al6.7'],loc='lower center',bbox_to_anchor=(0.5,1),ncol=2,frameon=False)

fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/v3/zt and kappa.pdf', bbox_inches='tight',format='pdf')
