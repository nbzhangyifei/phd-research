#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  4 20:26:38 2021

@author: yifeiz
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
from fractions import Fraction as frac
from matplotlib.ticker import ScalarFormatter
from sklearn.linear_model import LinearRegression
import math
from scipy import stats


color=['#d1e5f0',
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#b2182b'] 

rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman']
rcParams['figure.figsize'] = (6.8, 6)
rcParams['font.size'] = 12
rcParams['figure.titlesize'] = 18
rcParams['legend.fontsize'] = 12
rcParams['savefig.dpi'] = 300
linewidth=1


path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/mobility'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) 

fig = plt.figure(num=1,  figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
ax = plt.axes(xscale='log', yscale='log')
plt.xlim((360,950))
plt.ylim((9,50))
plt.tick_params(direction='in', width=1,top=True, right=True)
ax.tick_params('x', which='both', direction='in', width=1,top=True, right=True)
ax.tick_params('y', which='both', direction='in', width=1,top=True, right=True)
ax.xaxis.set_major_formatter(ScalarFormatter(useMathText=True))
ax.yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
##plt.ticklabel_format(axis='both', style='plain')
new_xticks=np.linspace(400,900, num=6)
new_yticks=np.linspace(10,50, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(ticks=new_yticks,fontfamily=rcParams['font.family'], fontsize=22)

# plt.tick_params(direction='in', width=1,top=True, right=True)

plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Weighted mobility ($\mathregular{cm^{2}V^{-1}s^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)


# for i in range(0,len(files)-1):
# for i in [0]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,1]
#       mobility = df1.loc[:,9]  
#       plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
#       fitx=np.array(np.log10(temperature[0:7]))
#       # fitx=fitx.reshape((-1,1))
#       fity=np.array(np.log10(mobility[0:7]))
#       res = stats.linregress(fitx, fity)
#       x = np.linspace(373,373+50*4, 20) ###mix scattering
#       y = 10**res.intercept*x**(res.slope)
#       plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
#       # print(res.rvalue)
#       print(res.stderr)
#       print(res.slope)
      
# for i in [1]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,1]
#       mobility = df1.loc[:,9]  
#       plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
#       fitx=np.array(np.log10(temperature[0:5]))
#       # fitx=fitx.reshape((-1,1))
#       fity=np.array(np.log10(mobility[0:5]))
#       res = stats.linregress(fitx, fity)
#       x = np.linspace(373,373+50*4, 20) ###mix scattering
#       y = 10**res.intercept*x**(res.slope)
#       plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
#       # print(res.rvalue)
#       print(res.stderr)
#       print(res.slope)
      
# for i in [2]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,1]
#       mobility = df1.loc[:,9]  
#       plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
#       fitx=np.array(np.log10(temperature[0:4]))
#       # fitx=fitx.reshape((-1,1))
#       fity=np.array(np.log10(mobility[0:4]))
#       res = stats.linregress(fitx, fity)
#       x = np.linspace(373,373+50*4, 20) ###mix scattering
#       y = 10**res.intercept*x**(res.slope)
#       plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
#       # print(res.rvalue)
#       print(res.stderr)
#       print(res.slope)
      
# for i in [3]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,1]
#       mobility = df1.loc[:,9]  
#       plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
#       fitx=np.array(np.log10(temperature[0:4]))
#       # fitx=fitx.reshape((-1,1))
#       fity=np.array(np.log10(mobility[0:4]))
#       res = stats.linregress(fitx, fity)
#       x = np.linspace(373,373+50*4, 20) ###mix scattering
#       y = 10**res.intercept*x**(res.slope)
#       plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
#       # print(res.rvalue)
#       print(res.stderr)
#       print(res.slope)
      
# for i in [4]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,1]
#       mobility = df1.loc[:,9]  
#       plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
#       fitx=np.array(np.log10(temperature[0:4]))
#       # fitx=fitx.reshape((-1,1))
#       fity=np.array(np.log10(mobility[0:4]))
#       res = stats.linregress(fitx, fity)
#       x = np.linspace(373,373+50*4, 20) ###mix scattering
#       y = 10**res.intercept*x**(res.slope)
#       plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
#       # print(res.rvalue)
#       print(res.stderr)
#       print(res.slope)
      
for i in [1]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[4:]))
      # fitx=fitx.reshape((-1,1))
      fity=np.array(np.log10(mobility[4:]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(573,873, 20) ###mix scattering
      y = 10**res.intercept*x**(res.slope)
      plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
      # print(res.rvalue)
      print(res.stderr)
      print(res.slope)
      
for i in [2]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[4:]))
      # fitx=fitx.reshape((-1,1))
      fity=np.array(np.log10(mobility[4:]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(573,873, 20) ###mix scattering
      y = 10**res.intercept*x**(res.slope)
      plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
      # print(res.rvalue)
      print(res.stderr)
      print(res.slope)
      
for i in [3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[4:]))
      # fitx=fitx.reshape((-1,1))
      fity=np.array(np.log10(mobility[4:]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(573,923, 20) ###mix scattering
      y = 10**res.intercept*x**(res.slope)
      plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
      # print(res.rvalue)
      print(res.stderr)
      print(res.slope)
      
for i in [4]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      plt.scatter(temperature,mobility, s=100,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[4:]))
      # fitx=fitx.reshape((-1,1))
      fity=np.array(np.log10(mobility[4:]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(573,923, 20) ###mix scattering
      y = 10**res.intercept*x**(res.slope)
      plt.plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
      # print(res.rvalue)
      print(res.stderr)
      print(res.slope)
   
      
      
plt.show()