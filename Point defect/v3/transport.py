import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
from fractions import Fraction as frac
from matplotlib.ticker import ScalarFormatter
from sklearn.linear_model import LinearRegression
from scipy import stats

color=['#d1e5f0',
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#b2182b'] 

rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman']
rcParams['figure.figsize'] = (6.8, 6.5)
rcParams['font.size'] = 12
rcParams['figure.titlesize'] = 18
rcParams['legend.fontsize'] = 12
rcParams['savefig.dpi'] = 300
linewidth=1




fig, axs = plt.subplots(2, 2, gridspec_kw= {'hspace':0.3,'wspace': 0.35})
xlim=[[-0.3,7],[360,950],[360,1000],[360,1000]]
ylim=[[-1.8,2],[10,58],[0,19],[-290,-40]]
xticks=[np.arange(0,8,1),np.arange(400,901,100),np.arange(400,1001,100),np.arange(400,1001,100)]
yticks=[np.arange(-1.5,1.6,0.5),np.arange(10,51,10),np.arange(0,17.6,2.5), np.arange(-250,-49,50)]

axs[0,0].set_xticks(xticks[0])
axs[0,0].set_yticks(yticks[0])
axs[0,0].set_xlim(xlim[0])
axs[0,0].set_ylim(ylim[0])

axs[0,0].set_xlabel('Al content per unit cell')
axs[0,0].set_ylabel('Exponent')



file = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/scattering mechanism.csv'
data = pd.read_csv(file,header=None)
df=pd.DataFrame(data)

for i in [0,1,2,3,4]: 
    com=df.loc[i,0]
    lowT=df.loc[i,1]
    lowTerr=df.loc[i,2]
    highT=df.loc[i,3]
    highTerr=df.loc[i,4]
    axs[0,0].scatter(com,lowT, s=50,marker='o', color=color[i])
    axs[0,0].errorbar(com,lowT,yerr=lowTerr, ls=None, color=color[i], lw=0,elinewidth=1)
    axs[0,0].scatter(com,highT, s=50,marker='d', color=color[i])
    axs[0,0].errorbar(com,highT,yerr=highTerr, ls=None, color=color[i], lw=0,elinewidth=1)


x1=[-0.3, 7]
y1=[1.5,1.5]
axs[0,0].plot(x1,y1,linewidth=1,linestyle='--', color='k',alpha=0.5)
y2=[-0.5, -0.5]
axs[0,0].plot(x1,y2,linewidth=1,linestyle='--', color='k',alpha=0.5)
y3=[-1.5,-1.5]
axs[0,0].plot(x1,y3,linewidth=1,linestyle='--', color='k',alpha=0.5)

axs[0,0].text(2.8,1.6,'Impurity scattering')
axs[0,0].text(-0.2,-0.45,'Alloy scattering')
axs[0,0].text(1,-1.7,'Acoustic phonon scattering')

custom_lines = [Line2D([0], [0], marker='o',color='k',lw=0),
                Line2D([0], [0], marker='d',color='k',lw=0),
]

axs[0,0].legend(custom_lines, ['low temperature','high temperature'],loc='lower center',bbox_to_anchor=(0.3,1), frameon=False)


axs[0,1].set_xticks(xticks[1])
axs[0,1].set_yticks(yticks[1])
axs[0,1].set_xlim(xlim[1])
axs[0,1].set_ylim(ylim[1])
axs[0,1].set_xscale('log')
axs[0,1].set_yscale('log')
axs[0,1].xaxis.set_major_formatter(ScalarFormatter(useMathText=True))
axs[0,1].xaxis.set_minor_formatter(ScalarFormatter(useMathText=True))
axs[0,1].yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
axs[0,1].yaxis.set_minor_formatter(ScalarFormatter(useMathText=True))
axs[0,1].set_xlabel('Temperature (K)')
axs[0,1].set_ylabel('Weighted mobility ($\mathregular{cm^{2}V^{-1}s^{-1}}$)')
path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/mobility'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime)
# for i in range(0,len(files)-1):
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,1]
#       mobility = df1.loc[:,9]  
#       axs[0,1].scatter(temperature,mobility, s=50,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))

for i in [0]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      axs[0,1].scatter(temperature,mobility, s=50,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[0:7]))
      fity=np.array(np.log10(mobility[0:7]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(373,373+50*4, 20)
      y = 10**res.intercept*x**(res.slope)
      axs[0,1].plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
for i in [1]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      axs[0,1].scatter(temperature,mobility, s=50,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[0:5]))
      fity=np.array(np.log10(mobility[0:5]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(373,373+50*4, 20) ###mix scattering
      y = 10**res.intercept*x**(res.slope)
      axs[0,1].plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
for i in [2]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      axs[0,1].scatter(temperature,mobility, s=50,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[0:4]))
      fity=np.array(np.log10(mobility[0:4]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(373,373+50*4, 20) ###mix scattering
      y = 10**res.intercept*x**(res.slope)
      axs[0,1].plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
      
path2 = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/mobility standard deviation'
extension = 'csv'
os.chdir(path2)
files2 = glob.glob('*.{}'.format(extension))
files2.sort(key=os.path.getmtime)

f2=files2[0]
data2 = pd.read_csv(f2,header=None)
df2=pd.DataFrame(data2)
error=df2.loc[:,2]
Al4=axs[0,1].errorbar(temperature,mobility, yerr=error, ls=None,color=color[2], lw=0,elinewidth=1, label='Al4.5')#errorbar of Al4.5

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/mobility'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime)
for i in [3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      axs[0,1].scatter(temperature,mobility, s=50,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[0:4]))
      fity=np.array(np.log10(mobility[0:4]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(373,373+50*4, 20) ###mix scattering
      y = 10**res.intercept*x**(res.slope)
      # axs[0,1].plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
for i in [4]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,1]
      mobility = df1.loc[:,9]  
      axs[0,1].scatter(temperature,mobility, s=50,marker='s', color=color[i], edgecolor=None, label=str(f.replace('.csv','')))
      fitx=np.array(np.log10(temperature[0:4]))
      fity=np.array(np.log10(mobility[0:4]))
      res = stats.linregress(fitx, fity)
      x = np.linspace(373,373+50*4, 20) ###mix scattering
      y = 10**res.intercept*x**(res.slope)
      axs[0,1].plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
      
path2 = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/mobility standard deviation'
extension = 'csv'
os.chdir(path2)
files2 = glob.glob('*.{}'.format(extension))
files2.sort(key=os.path.getmtime)     
f2=files2[1]
data2 = pd.read_csv(f2,header=None)
df2=pd.DataFrame(data2)
error=df2.loc[:,2]
Al4=axs[0,1].errorbar(temperature[:12],mobility[:12], yerr=error[:12], ls=None,color=color[4], lw=0,elinewidth=1, label='Al6.7')#errorbar of Al6.7

x = np.linspace(623,923, 20) ###mix scattering
y = 600000*x**(-1.5)
axs[0,1].plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
axs[0,1].text(700,35,'$\mathregular{T^{-3/2}}$')

x = np.linspace(623,923, 20) ###mix scattering
y = 600*x**(-0.5)
axs[0,1].plot(x,y, linestyle='dashed', linewidth='1', color='black', alpha=0.5)
axs[0,1].text(700,17,'$\mathregular{T^{-1/2}}$')


axs[0,1].text(400,42,'$\mathregular{T^{-0.81}}$')
axs[0,1].text(400,10.4,'$\mathregular{T^{1.31}}$')
axs[0,1].text(400,28,'$\mathregular{T^{0.6}}$')
axs[0,1].text(400,15.5,'$\mathregular{T^{1.14}}$')

custom_lines = [Line2D([0], [0],color=color[0], lw=6),
                Line2D([0], [0], color=color[1], lw=6),
                Line2D([0], [0],color=color[2], lw=6),
                Line2D([0], [0], color=color[3], lw=6),
                Line2D([0], [0], color=color[4], lw=6)]

axs[0,1].legend(custom_lines, ['Al0.0','Al3.4','Al4.5','Al5.8','Al6.7'],loc='lower center',bbox_to_anchor=(0.3,1), ncol=3,frameon=False)


axs[1,0].set_xticks(xticks[2])
axs[1,0].set_yticks(yticks[2])
axs[1,0].set_xlim(xlim[2])
axs[1,0].set_ylim(ylim[2])
axs[1,0].set_xlabel('Temperature (K)')
axs[1,0].set_ylabel('Resistivity (m$\Omega$cm)')

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/transport'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime)
for i in range(0,len(files)-1):
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
      axs[1,0].scatter(temperature, resistivity, s=50,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))


axs[1,1].set_xticks(xticks[3])
axs[1,1].set_yticks(yticks[3])
axs[1,1].set_xlim(xlim[3])
axs[1,1].set_ylim(ylim[3])
axs[1,1].set_xlabel('Temperature (K)')
axs[1,1].set_ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)')
for i in range(0,len(files)-1):
      f=files[i]
      data = pd.read_csv(f, header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      seebeck = df1.loc[:,2]*1e6 #transfer unit to uV/K
      axs[1,1].scatter(temperature, seebeck, s=50, marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/XRD'
# extension = 'csv'
# os.chdir(path)
# files = glob.glob('*.{}'.format(extension))
# files.sort(key=os.path.getmtime) #sort file according to modified time

# for i in [0,1,2,3,4]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
#       inten = df1.loc[:,1]-1.1*i
#       ax1.plot(theta, inten, color=color[i], linewidth=1,label=None)
     
# for i in [6]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       theta = df1.loc[:,0]
#       inten = df1.loc[:,1]
#       plane = str(df1.loc[:,2])
#       ax1.vlines(x=theta, ymin=-5.2, ymax=-4.7,colors='k', linewidth=1,label=None)


# ax1.annotate('Ge', xy = (27,-1), xytext = (24.5, -0.5),arrowprops=dict(facecolor='black', width=1, headwidth=1.5, headlength=1.5))


# ax2.set_xticks(xticks[1])
# ax2.set_xlim(xlim[1])

# ax2.set_xlabel('Al content per unit cell')
# ax2.set_ylabel('Lattice parameter (Å)')


# f= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/unit cell as composition.csv'
# data = pd.read_csv(f,header=None)
# df=pd.DataFrame(data)


# for i in range(0,5):
#   comXRF=df.loc[i, 0]
#   comXRD=df.loc[i,1]
#   lat=df.loc[i, 2]
#   error=df.loc[i,3]
#   name=df.loc[i,4]
#   ax2.scatter(comXRD,lat,marker='s',s=500,color=color[i],edgecolor=color[i], linewidth=1,label=name)
  
# x1=[df.loc[0,1],16]
# y1=[10.7802,10.815]

# x2=[df.loc[0,1],16]
# y2=[10.7828,10.85335]
# ##plt.plot(x1,y1,linestyle='solid', linewidth='1', color=None,alpha=0.5)
# ##plt.plot(x2,y2,linestyle='solid', linewidth='1', color='orange',alpha=0.5)
# ax2.fill_between(x1,y1,y2,facecolor='orange',alpha=0.4)


# f2= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/structure study/unit cell as composition reference.csv'
# data2 = pd.read_csv(f2,header=None)
# df2=pd.DataFrame(data2)
# composition2 = df2.loc[:,0]
# lp2 = df2.loc[:,3]#from Chr_JACS2006, n-type BaGaGe by Czochralski, single crystal XRD Mo Ka radiation
# ax2.scatter(composition2,lp2,s=500, marker='s', color='white',edgecolor='black', linewidth=1,label=None)
# lp2 = df2.loc[:,4]#from Chr_CM2007, BaAlGe by Czpchralski, single crystal XRD Mo Ka radiation
# ax2.scatter(composition2,lp2,s=500, marker='s', color='white',edgecolor='black', linewidth=1,label=None)
# # lp = df1.loc[:,7]#from AndRan:applied materials and interfaces_2018, BaAlGe by arc melting, powder XRD Mo Ka radiation
# # plt.scatter(composition,lp,s=50**2, marker='s', color='white',edgecolor='black', label=None)


# lp2 = df2.loc[:,5]#from Chr_CM2007, BaAlGe by normal shake and bake, single crystal XRD Mo Ka radiation
# ax2.scatter(composition2,lp2,s=500, marker='s', color='white',edgecolor='black', linewidth=1,label=None)
# lp2 = df2.loc[:,6]#from Chr_CM2007, BaAlGe by Al flux, single crystal XRD Mo Ka radiation
# ax2.scatter(composition2,lp2,s=500, marker='s', color='white',edgecolor='black', linewidth=1,label=None)

# f3= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/structure study/unit cell as composition.csv' #reference from other flux-grown samples
# data3 = pd.read_csv(f3,header=None)
# df3=pd.DataFrame(data3)

# for i in [1,2,4]:
#   comXRD3=df3.loc[i,1]
#   lat3=df3.loc[i, 2]
#   ax2.scatter(comXRD3,lat3,marker='s',s=500,color='white',edgecolor='black',linewidth=1, label=None)



# # ax2.legend(loc='upper left',shadow=None,facecolor=None, edgecolor='black')

# custom_lines = [Line2D([0], [0], color=color[0], lw=6),
#                 Line2D([0], [0], color=color[1], lw=6),
#                 Line2D([0], [0], color=color[2], lw=6),
#                 Line2D([0], [0], color=color[3], lw=6),
#                 Line2D([0], [0], color=color[4], lw=6)]

# ax2.legend(custom_lines, ['Al0.0','Al3.4','Al4.5','Al5.8','Al6.7'],bbox_to_anchor=(0.5,0.9), frameon=False)

axs[0,0].text(0.02,0.98,'(a)', transform=axs[0,0].transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')
axs[0,1].text(0.02,0.98,'(b)', transform=axs[0,1].transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')
axs[1,0].text(0.02,0.98,'(c)', transform=axs[1,0].transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')
axs[1,1].text(0.02,0.98,'(d)', transform=axs[1,1].transAxes,
                            horizontalalignment='left',
                            verticalalignment='top')


fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/v3/transport.pdf', format='pdf',bbox_inches='tight')

