import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob


#col1, temperature (°C)
#col2, resistivity (Ohm*m)
#col3, Seebeck (V/K)
#col4, power factor (W/mK^2)


# # color=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
#               '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
#               '#bcbd22', '#17becf']
# color=['#e0f3db',
# '#ccebc5',
# '#a8ddb5',
# '#7bccc4',
# '#2b8cbe',
# '#2b8cbe',
# '#0868ac',
# '#084081']
color=['#d1e5f0',
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#b2182b'
       ] 
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/CeBAGG/transport'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((360,1000))
# plt.ylim((0,10))
new_xticks=np.linspace(400,1000, num=7)
##new_yticks=np.linspace(0.8,2, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)
plt.tick_params(direction='in', width=1,top=True, right=True)
plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Resistivity (m$\Omega$cm)', fontfamily=rcParams['font.family'], fontsize=22)


for i in [0]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
      plt.scatter(temperature, resistivity, s=100,marker='s',color='#2166ac',edgecolor=None, label=str(f.replace('.csv','')))
for i in [1]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
      plt.scatter(temperature, resistivity, s=100,marker='s',color='#ef8a62',edgecolor=None, label=str(f.replace('.csv','')))
     
# for i in [2]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
#       plt.scatter(temperature, resistivity, s=6**2,marker='o',color='w',edgecolor='#2166ac', label=str(f.replace('.csv','')))
for i in [3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
      plt.scatter(temperature, resistivity, s=100,marker='D',color='w',edgecolor='#2166ac', label=str(f.replace('.csv','')))
for i in [4]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
      plt.scatter(temperature, resistivity, s=100,marker='D',color='w',edgecolor='#ef8a62', label=str(f.replace('.csv','')))


plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/Ce resistivity.pdf', format='pdf')

fig = plt.figure(num=2, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
plt.xlim((360,1000))
new_xticks=np.linspace(400,1000, num=7)
##new_yticks=np.linspace(0.8,2, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)
plt.tick_params(direction='in', width=1,top=True, right=True)
plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)


for i in [0]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      seebeck = df1.loc[:,2]*1e6  #transfer unit to mOhm*cm
      plt.scatter(temperature, seebeck, s=100,marker='s',color='#2166ac',edgecolor=None, label=str(f.replace('.csv','')))
for i in [1]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      seebeck = df1.loc[:,2]*1e6  #transfer unit to mOhm*cm
      plt.scatter(temperature, seebeck, s=100,marker='s',color='#ef8a62',edgecolor=None, label=str(f.replace('.csv','')))
# for i in [2]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       seebeck = df1.loc[:,2]*1e6  #transfer unit to mOhm*cm
#       plt.scatter(temperature, seebeck, s=6**2,marker='o',color='w',edgecolor='#2166ac', label=str(f.replace('.csv','')))
for i in [3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      seebeck = df1.loc[:,2]*1e6  #transfer unit to mOhm*cm
      plt.scatter(temperature, seebeck, s=100,marker='D',color='w',edgecolor='#2166ac', label=str(f.replace('.csv','')))
for i in [4]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      seebeck = df1.loc[:,2]*1e6  #transfer unit to mOhm*cm
      plt.scatter(temperature, seebeck, s=100,marker='D',color='w',edgecolor='#ef8a62', label=str(f.replace('.csv','')))


# for i in range(0,len(files)):
#       f=files[i]
#       data = pd.read_csv(f, header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       seebeck = df1.loc[:,2]*1e6 #transfer unit to uV/K
#       plt.scatter(temperature, seebeck, s=6**2, marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/Ce seebeck.pdf', format='pdf')




path2 = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/CeBAGG/power factor'
extension = 'csv'
os.chdir(path2)
files2 = glob.glob('*.{}'.format(extension))
files2.sort(key=os.path.getmtime) #sort file according to modified time

fig = plt.figure(num=3, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)

plt.xlim((360,1000))
# plt.ylim((0,2))
# plt.tick_params(direction='in', width=1,top=True, right=True)
ax.tick_params('x', which='both', direction='in', width=1,top=True, right=True)
ax.tick_params('y', which='both', direction='in', width=1,top=True, right=True)

for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)
##plt.ticklabel_format(axis='both', style='plain')
new_xticks=np.linspace(400,1000, num=7)
# new_yticks=np.linspace(10,60, num=6)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)

# plt.tick_params(direction='in', width=1,top=True, right=True)

plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Power factor ($\mathregular{mWm^{-1}K^{-2}}$)', fontfamily=rcParams['font.family'], fontsize=22)


for i in [2,3,4]:
      f2=files2[i]
      data2 = pd.read_csv(f2,header=None)
      df2=pd.DataFrame(data2)
      temperature2 = df2.loc[:,0]+273.15
      pf2 = df2.loc[:,2]*df2.loc[:,2]/df2.loc[:,1]*1000
      plt.scatter(temperature2,pf2, s=100,marker='s', color=color[i], edgecolor=None, label=str(f2.replace('.csv','')))

for i in [6]:
      f2=files2[i]
      data2 = pd.read_csv(f2,header=None)
      df2=pd.DataFrame(data2)
      temperature2 = df2.loc[:,0]+273.15
      pf2 = df2.loc[:,2]*df2.loc[:,2]/df2.loc[:,1]*1000
      plt.scatter(temperature2,pf2, s=100,marker='D', color='w', edgecolor='#2166ac', label=str(f2.replace('.csv','')))

for i in [7]:
      f2=files2[i]
      data2 = pd.read_csv(f2,header=None)
      df2=pd.DataFrame(data2)
      temperature2 = df2.loc[:,0]+273.15
      pf2 = df2.loc[:,2]*df2.loc[:,2]/df2.loc[:,1]*1000
      plt.scatter(temperature2,pf2, s=100,marker='D', color='w', edgecolor='#ef8a62', label=str(f2.replace('.csv','')))



plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/Ce power factor.pdf', format='pdf')
plt.show()