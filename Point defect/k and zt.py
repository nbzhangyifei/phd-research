#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 15:55:11 2020

@author: yifeiz
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob

color=[
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62',
'#2166ac','#ef8a62'
        ]
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/k and zt'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig, (ax1, ax2,ax3) = plt.subplots(3, dpi=dpi,sharex=True, gridspec_kw={'hspace': 0}, figsize=(6.4,9.6))
# fig, (ax1, ax2,ax3) = plt.subplots(3, dpi=dpi,sharex=True, gridspec_kw={'hspace': 0}, figsize=(6.4,6.4))
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(1.5)
  ax2.spines[axis].set_linewidth(1.5)
  ax3.spines[axis].set_linewidth(1.5)

new_xticks=np.linspace(400,1000, num=7)
new_yticks=np.linspace(0.8,2.6, num=4)

ax1.axis(xmin=360, xmax=1000)
ax1.tick_params(direction='in', width=1,top=True, right=True)
ax2.tick_params(direction='in', width=1,top=True, right=True)
ax3.tick_params(direction='in', width=1,top=True, right=True)

ax1.tick_params(axis='y', labelsize=22)
ax2.tick_params(axis='y', labelsize=22)
ax3.tick_params(axis='y', labelsize=22)

ax3.tick_params(axis='x', labelsize=22)
ax3.set_xticks(new_xticks)
ax1.set_yticks(new_yticks)

ax1.set_ylabel('$\mathit{\kappa}$ ($\mathregular{Wm^{-1}K^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
ax2.set_ylabel('Power factor ($\mathregular{mWm^{-1}K^{-2}}$)', fontfamily=rcParams['font.family'], fontsize=22)
ax3.set_ylabel('Figure of merit', fontfamily=rcParams['font.family'], fontsize=22)
ax3.set_xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)

for i in [0,1,2,3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      k = df1.loc[:,6]
      error=df1.loc[:,7]
      ax1.errorbar(temperature,k,yerr=error,fmt='s',ms=10,color=color[i],edgecolor=None)
      kl = df1.loc[:,8]
      # ax2.scatter(temperature, kl,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
      zt=df1.loc[:,9]
      ax3.scatter(temperature, zt,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

ax3.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

path2 = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/transport'
extension = 'csv'
os.chdir(path2)
files2 = glob.glob('*.{}'.format(extension))
files2.sort(key=os.path.getmtime)

for i in [1,2,3,4]:
      f2=files2[i]
      data2 = pd.read_csv(f2, header=None)
      df2=pd.DataFrame(data2)
      temperature2 = df2.loc[:,0]+273.15
      pf2 = df2.loc[:,3]*1e3 #transfer unit to mW/mK
      ax2.scatter(temperature2, pf2, s=100, marker='s',color=color[i-1],edgecolor=None, label=str(f.replace('.csv','')))
# ax2.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')

plt.tight_layout()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/zt and kappa.pdf', format='pdf')
# fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
# ax = fig.add_subplot(111)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)

# plt.xlim((360,1000))
# plt.ylim((0.6,3.3))
# # plt.tick_params(direction='in', width=1,top=True, right=True)
# ax.tick_params('x', which='both', direction='in', width=1,top=True, right=True)
# ax.tick_params('y', which='both', direction='in', width=1,top=True, right=True)

# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)
# ##plt.ticklabel_format(axis='both', style='plain')
# new_xticks=np.linspace(400,1000, num=7)
# # new_yticks=np.linspace(10,60, num=6)
# plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)

# # plt.tick_params(direction='in', width=1,top=True, right=True)

# plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
# plt.ylabel('$\mathit{\kappa}$ ($\mathregular{Wm^{-1}K^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)

# for i in [0,1,2,3]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       k = df1.loc[:,6]
#       error=df1.loc[:,7]
# ##      plt.scatter(temperature, k,s=4**2,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
#       plt.errorbar(temperature,k,yerr=error,fmt='s',ms=10,color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# for i in [4,5]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       k = df1.loc[:,6]
#       error=df1.loc[:,7]
# #      plt.scatter(temperature, k,s=4**2,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
#       plt.errorbar(temperature,k,yerr=error,fmt='D',ms=10,color='white',ecolor=color[i],markeredgecolor=color[i], label=str(f.replace('.csv','')))

# plt.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# plt.tight_layout()
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/kappa.pdf', format='pdf')

# fig = plt.figure(num=2, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
# ax = fig.add_subplot(111)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)

# plt.xlim((360,1000))
# # plt.ylim((0,2))
# # plt.tick_params(direction='in', width=1,top=True, right=True)
# ax.tick_params('x', which='both', direction='in', width=1,top=True, right=True)
# ax.tick_params('y', which='both', direction='in', width=1,top=True, right=True)

# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)
# ##plt.ticklabel_format(axis='both', style='plain')
# new_xticks=np.linspace(400,1000, num=7)
# # new_yticks=np.linspace(10,60, num=6)
# plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)

# # plt.tick_params(direction='in', width=1,top=True, right=True)

# plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
# plt.ylabel('$\mathit{\kappa}_{l}$ ($\mathregular{Wm^{-1}K^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)

# for i in [0,1,2,3]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       kl = df1.loc[:,8]
#       plt.scatter(temperature, kl,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
#       # plt.errorbar(temperature,kl,yerr=error,fmt='s',ms=6,color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
      
# # for i in [4,5]:
# #       f=files[i]
# #       data = pd.read_csv(f,header=None)
# #       df1=pd.DataFrame(data)
# #       temperature = df1.loc[:,0]+273.15
# #       kl = df1.loc[:,8]
# #       plt.scatter(temperature, kl,s=100,marker='D',color='white',edgecolor=color[i], label=str(f.replace('.csv','')))

# # plt.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# plt.tight_layout()
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/kappa_l.pdf', format='pdf')

# fig = plt.figure(num=3, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
# ax = fig.add_subplot(111)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)

# plt.xlim((360,1000))
# # plt.ylim((0,2))
# # plt.tick_params(direction='in', width=1,top=True, right=True)
# ax.tick_params('x', which='both', direction='in', width=1,top=True, right=True)
# ax.tick_params('y', which='both', direction='in', width=1,top=True, right=True)

# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)
# ##plt.ticklabel_format(axis='both', style='plain')
# new_xticks=np.linspace(400,1000, num=7)
# # new_yticks=np.linspace(10,60, num=6)
# plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)

# # plt.tick_params(direction='in', width=1,top=True, right=True)

# plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
# plt.ylabel('Figure of merit', fontfamily=rcParams['font.family'], fontsize=22)

# for i in [0,1,2,3]:
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       kl = df1.loc[:,9]
#       plt.scatter(temperature, kl,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
#       # plt.errorbar(temperature,kl,yerr=error,fmt='s',ms=6,color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# # for i in [4,5]:
# #       f=files[i]
# #       data = pd.read_csv(f,header=None)
# #       df1=pd.DataFrame(data)
# #       temperature = df1.loc[:,0]+273.15
# #       kl = df1.loc[:,9]
# #       plt.scatter(temperature, kl,s=100,marker='D',color='white',edgecolor=color[i], label=str(f.replace('.csv','')))

# plt.legend(loc='upper left', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# plt.tight_layout()
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/zt.pdf', format='pdf')

plt.show()