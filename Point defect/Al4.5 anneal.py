import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob


#col1, temperature (°C)
#col2, resistivity (Ohm*m)
#col3, Seebeck (V/K)
#col4, power factor (W/mK^2)


# # color=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
#               '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
#               '#bcbd22', '#17becf']
# color=['#e0f3db',
# '#ccebc5',
# '#a8ddb5',
# '#7bccc4',
# '#2b8cbe',
# '#2b8cbe',
# '#0868ac',
# '#084081']
# color=['#d1e5f0',
# '#67a9cf',
# '#2166ac',
# '#fee090',
# '#ef8a62',
# '#b2182b'
#        ]
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/Al4.5 anneal'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

# fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
# ax = fig.add_subplot(111)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)
# plt.xlim((360,1000))
# plt.ylim((0,18))
# new_xticks=np.linspace(400,1000, num=7)
# ##new_yticks=np.linspace(0.8,2, num=5)
# plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=18)
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)
# plt.tick_params(direction='in', width=1,top=True, right=True)
# plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=18)
# plt.ylabel('Resistivity (m$\Omega$cm)', fontfamily=rcParams['font.family'], fontsize=18)


# for i in range(0,len(files)):
#       f=files[i]
#       data = pd.read_csv(f,header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
#       plt.scatter(temperature, resistivity, s=6**2,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# plt.tight_layout()
# plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# plt.tight_layout()
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/resistivity.pdf', format='pdf')

# fig = plt.figure(num=2, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
# ax = fig.add_subplot(111)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(1.5)
# plt.xlim((360,1000))
# new_xticks=np.linspace(400,1000, num=7)
# ##new_yticks=np.linspace(0.8,2, num=5)
# plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=18)
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)
# plt.tick_params(direction='in', width=1,top=True, right=True)
# plt.xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=18)
# plt.ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=18)
# for i in range(0,len(files)):
#       f=files[i]
#       data = pd.read_csv(f, header=None)
#       df1=pd.DataFrame(data)
#       temperature = df1.loc[:,0]+273.15
#       seebeck = df1.loc[:,2]*1e6 #transfer unit to uV/K
#       plt.scatter(temperature, seebeck, s=6**2, marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

# plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# plt.tight_layout()
# # fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/seebeck.pdf', format='pdf')
# fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
fig, ax1 = plt.subplots(dpi=dpi,figsize=(6.4, 4.8) , facecolor='w', edgecolor='w' )
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(1.5)
plt.xlim((360,1000))
# plt.ylim((0,18))
new_xticks=np.linspace(400,1000, num=7)
##new_yticks=np.linspace(0.8,2, num=5)
plt.xticks(ticks=new_xticks,fontfamily=rcParams['font.family'], fontsize=22)
plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)
plt.tick_params(direction='in', width=1,top=True, right=True)
color = 'red'
ax1.set_xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)
ax1.set_ylabel('Resistivity (m$\Omega$cm)', fontfamily=rcParams['font.family'], fontsize=22,color=color)

# f=files[0]
# data = pd.read_csv(f,header=None)
# df1=pd.DataFrame(data)
# temperature = df1.loc[:,0]+273.15
# resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
# plt.scatter(temperature, resistivity, s=6**2,marker='s',color=color,edgecolor=None, label=str(f.replace('.csv','')))
# ax1.tick_params(axis='y', labelcolor=color)

f=files[1]
data = pd.read_csv(f,header=None)
df1=pd.DataFrame(data)
temperature = df1.loc[:,0]+273.15
resistivity = df1.loc[:,1]*1e5  #transfer unit to mOhm*cm
plt.scatter(temperature, resistivity, s=100,marker='s',color='white',edgecolor=color, label=str(f.replace('.csv','')))
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'blue'
plt.yticks(fontfamily=rcParams['font.family'], fontsize=22)
plt.tick_params(direction='in', width=1,top=True, right=True)
ax2.set_ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22, color=color)  # we already handled the x-label with ax1

# f=files[0]
# data = pd.read_csv(f,header=None)
# df1=pd.DataFrame(data)
# temperature = df1.loc[:,0]+273.15
# seebeck = df1.loc[:,2]*1e6  
# plt.scatter(temperature, seebeck, s=6**2,marker='s',color=color,edgecolor=None, label=str(f.replace('.csv','')))


f=files[1]
data = pd.read_csv(f,header=None)
df1=pd.DataFrame(data)
temperature = df1.loc[:,0]+273.15
seebeck = df1.loc[:,2]*1e6  
plt.scatter(temperature, seebeck, s=100,marker='s',color='white',edgecolor=color, label=str(f.replace('.csv','')))

ax2.tick_params(axis='y', labelcolor=color)

# plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()
plt.show()
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/Al4.5 anneal.pdf', format='pdf')
