#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 16:56:10 2020

@author: yifeiz
"""

f= '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/XRD Al3.4.csv'

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob


dpi=300

rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] 

fig = plt.figure(num=1, dpi=dpi, figsize=(6.4, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)

plt.xlim((5,60))
##plt.ylim((1,3))
# new_xticks=np.linspace(0,16, num=9)
##new_yticks=np.linspace(1,3, num=5)
plt.xticks(fontfamily=rcParams['font.family'], fontsize=18)

ax.set_yticks([])
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=0)
##plt.xticks(fontfamily=rcParams['font.family'], fontsize=18)
# plt.yticks(fontfamily=rcParams['font.family'], fontsize=18)
plt.tick_params(direction='in', width=1,top=True, right=True)

plt.xlabel('2\u03B8 (°)', fontfamily=rcParams['font.family'], fontsize=22)
plt.ylabel('Intensity (a. u.)', fontfamily=rcParams['font.family'], fontsize=22)



data = pd.read_csv(f,header=None)
df=pd.DataFrame(data)

xca=df.loc[:,2]
yca=df.loc[:,3]
plt.plot(xca,yca, linewidth=1, color='black',label='cal.')

xob=df.loc[:,0]
yob=df.loc[:,1]
plt.scatter(xob,yob, s=9, color = 'white', edgecolor = 'red', label='obs.')

xdif=df.loc[:,4]
ydif=df.loc[:,5]+200
plt.plot(xdif,ydif, linewidth=1, color='blue',label='obs. - cal.')

xbra=df.loc[:,6]
ybra=df.loc[:,7]
plt.vlines(x=xbra, ymin=-205.08951, linewidth=1,ymax=0,colors='blue',label='Bragg Peaks')

plt.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')


plt.text(30, 4000,'R = 3.1',fontdict={'size':18, 'color':'black'})
plt.text(30, 3700,'wR = 2.9',fontdict={'size':18, 'color':'black'})
plt.text(30, 3400,'chi2 = 2.5',fontdict={'size':18, 'color':'black'})
plt.tight_layout()

plt.show()
fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/XRD_Al3.4.pdf', format='pdf')