import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob

from scipy.interpolate import make_interp_spline, BSpline


dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

color=[
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62'

       ]

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/point defect/Cp and heat flow'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig, (ax1, ax2) = plt.subplots(2, dpi=dpi,sharex=True, gridspec_kw={'hspace': 0}, figsize=(6.4,9.6))

# fig = plt.figure(num=1, dpi=dpi, figsize=(12.8, 4.8) , facecolor='w', edgecolor='w')
ax = fig.add_subplot(111)
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(1.5)
  ax2.spines[axis].set_linewidth(1.5)

new_xticks=np.linspace(300,800, num=6)
##new_yticks=np.linspace(0.8,2, num=5)
ax2.set_xticks(new_xticks)

ax1.tick_params(direction='in', width=1,top=True, right=True)
ax2.tick_params(direction='in', width=1,top=True, right=True)
ax1.tick_params(axis='y', labelsize=22)
ax2.tick_params(axis='y', labelsize=22)

ax1.set_ylabel('Heat flow (mW)', fontfamily=rcParams['font.family'], fontsize=22)
ax2.set_ylabel('$c_p$ ($\mathregular{Jg^{-1}K^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
ax2.set_xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)






for i in range(0,len(files)):
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature1 =df1.loc[:,0]+273.15
      cp1 =df1.loc[:,1]
      temperature2 =df1.loc[:,2]+273.15
      hf =df1.loc[:,3]
      ax1.plot(temperature2, hf,color=color[i],linewidth=2,label=str(f.replace('.csv','')))
      ax2.plot(temperature1, cp1,color=color[i],linewidth=2,label=str(f.replace('.csv','')))

plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()                                                        
##plt.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/point defect/Cp.pdf', format='pdf')




plt.show()
