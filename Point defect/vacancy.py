#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 15:55:11 2020

@author: yifeiz
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

color=[

'#2166ac',
'#fee090',
'#ef8a62',
'#2166ac','#ef8a62'
        ]
dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/Point defect/Al4.5 anneal'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig, (ax1, ax3) = plt.subplots(2, dpi=dpi,sharex=True, gridspec_kw={'hspace': 0}, figsize=(6.4,7.2))
# fig, (ax1, ax2,ax3) = plt.subplots(3, dpi=dpi,sharex=True, gridspec_kw={'hspace': 0}, figsize=(6.4,6.4))
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(1.5)
  # ax2.spines[axis].set_linewidth(1.5)
  ax3.spines[axis].set_linewidth(1.5)

new_xticks=np.linspace(400,1000, num=7)
# new_yticks=np.linspace(0.8,2.6, num=4)

ax1.axis(xmin=360, xmax=1000)
ax1.tick_params(direction='in', width=1,top=True, right=True)
# ax2.tick_params(direction='in', width=1,top=True, right=True)
ax3.tick_params(direction='in', width=1,top=True, right=True)

ax1.tick_params(axis='y', labelsize=22)
# ax2.tick_params(axis='y', labelsize=22)
ax3.tick_params(axis='y', labelsize=22)

ax3.tick_params(axis='x', labelsize=22)
ax3.set_xticks(new_xticks)
# ax1.set_yticks(new_yticks)

ax1.set_ylabel('Resistivity (m$\Omega$cm)', fontfamily=rcParams['font.family'], fontsize=22)
# ax2.set_ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
ax3.set_ylabel('Seebeck coefficient ($\mathregular{\u03BCVK^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
# ax3.set_ylabel('Weighted mobility ($\mathregular{cm^{2}V^{-1}s^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
ax3.set_xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)

for i in [0,1,2]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      rho = df1.loc[:,1]*1e5
      see=df1.loc[:,2]*1e6
      uw = df1.loc[:,8]
      ax1.scatter(temperature, rho,s=100,marker='s',color=color[i],edgecolor=None)
      # ax2.scatter(temperature, see,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
      ax3.scatter(temperature, see,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))

for i in [3]:
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature = df1.loc[:,0]+273.15
      rho = df1.loc[:,1]*1e5
      see=df1.loc[:,2]*1e6
      uw = df1.loc[:,8]
      ax1.scatter(temperature, rho,s=100,marker='s',color='w',edgecolor='#2166ac')
      # ax2.scatter(temperature, see,s=100,marker='s',color=color[i],edgecolor=None, label=str(f.replace('.csv','')))
      ax3.scatter(temperature, see,s=100,marker='s',color='w',edgecolor='#2166ac', label='Al4.5 annealed')

ax3.legend(loc='upper right', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
ax1.annotate('Vacancy-induced transition', xy = (519,8.9), xytext = (460,7), fontsize=12,arrowprops=dict(edgecolor='#2166ac', width=2, headwidth=4, headlength=5))


plt.tight_layout()
# fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/Point defect/vacancy.pdf', format='pdf')


plt.show()