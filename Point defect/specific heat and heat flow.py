import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from matplotlib import rcParams
import os
import matplotlib.style
from cycler import cycler
import glob
from matplotlib.patches import Ellipse
from scipy.interpolate import make_interp_spline, BSpline


dpi=300
rcParams['font.family']='serif'
rcParams['font.serif']=['Times New Roman'] ###define font

color=[
'#67a9cf',
'#2166ac',
'#fee090',
'#ef8a62'

       ]

path = '/Users/yifeiz/Documents/PhD/Data/matplotlib/data/point defect/Cp and heat flow'
extension = 'csv'
os.chdir(path)
files = glob.glob('*.{}'.format(extension))
files.sort(key=os.path.getmtime) #sort file according to modified time

fig, (ax1, ax2) = plt.subplots(2, dpi=dpi,sharex=True, gridspec_kw={'hspace': 0}, figsize=(6.4,6.4))

for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(1.5)
  ax2.spines[axis].set_linewidth(1.5)

new_xticks=np.linspace(300,800, num=6)
##new_yticks=np.linspace(0.8,2, num=5)
ax2.set_xticks(new_xticks)

ax1.tick_params(direction='in', width=1,top=True, right=True)
ax2.tick_params(direction='in', width=1,top=True, right=True)
ax1.tick_params(axis='y', labelsize=22)
ax2.tick_params(axis='y', labelsize=22)
ax2.tick_params(axis='x', labelsize=22)

ax1.set_ylabel('Heat flow (mW)', fontfamily=rcParams['font.family'], fontsize=22)
ax2.set_ylabel('$c_p$ ($\mathregular{Jg^{-1}K^{-1}}$)', fontfamily=rcParams['font.family'], fontsize=22)
ax2.set_xlabel('Temperature (K)', fontfamily=rcParams['font.family'], fontsize=22)






for i in range(0,len(files)):
      f=files[i]
      data = pd.read_csv(f,header=None)
      df1=pd.DataFrame(data)
      temperature1 =df1.loc[:,0]+273.15
      cp1 =df1.loc[:,1]
      temperature2 =df1.loc[:,2]+273.15
      hf =df1.loc[:,3]
      ax1.plot(temperature2, hf,color=color[i],linewidth=2,label=str(f.replace('.csv','')))
      ax2.plot(temperature1, cp1,color=color[i],linewidth=2,label=str(f.replace('.csv','')))

ax1.legend(loc='best', fontsize=12,shadow=None,facecolor=None, edgecolor='black')
plt.tight_layout()   
                                                     
ax1.annotate(' ', xy = (519,45), xytext = (460,52), fontsize=12,arrowprops=dict(edgecolor='#2166ac', width=2, headwidth=4, headlength=5))
ax1.annotate(' ', xy = (685,57), xytext = (670,61), fontsize=22,arrowprops=dict(edgecolor='#2166ac', width=2, headwidth=4, headlength=5))

ax1.annotate(' ', xy = (700,51), xytext = (685,55), fontsize=22,arrowprops=dict(edgecolor='#67a9cf', width=2, headwidth=4, headlength=5))

ax1.annotate(' ', xy = (685,43), xytext = (670,47), fontsize=22,arrowprops=dict(edgecolor='#ef8a62', facecolor='#ef8a62', width=2, headwidth=4, headlength=5))
ax1.annotate(' ', xy = (615,37), xytext = (600,41), fontsize=22,arrowprops=dict(edgecolor='#fee090', facecolor='#fee090', width=2, headwidth=4, headlength=5))

circle1 = plt.Circle((685, 50),radius=18,edgecolor='black',facecolor='w',linewidth=2,alpha=0.7)
ax1.add_patch(circle1)
circle2 = plt.Circle((610, 35),radius=8,edgecolor='black',facecolor='w',linewidth=2,alpha=0.7)
ax1.add_patch(circle2)
# ax1.annotate('Vacancy-induced transition', xy = (519,45), xytext = (460,52), fontsize=12,arrowprops=dict(edgecolor='#2166ac', width=2, headwidth=4, headlength=5))

fig.savefig('/Users/yifeiz/Documents/PhD/Data/matplotlib/plot/point defect/Cp and heat flow.pdf', format='pdf')




plt.show()
